# Niederösterreichische Tourism Office

* Fictitious project, but based on a real project
* The „Niederösterreichische Tourism Office GmbH (NÖ-TO) supports the tourism industry in Lower Austria by collecting relevant information and providing statistics and other specific information to the industry to support their business. In addition, the office runs national and international marketing campaigns and provides consulting to tourism companies (hotels, restaurants, etc.) in Lower Austria
* One key offering is the „NÖ Tourism Portal“ (NÖ-TO Portal), an application (in German) that provides statistics about the Austrian tourist sector to registered tourism companies
* The application based on Java is out of date, there is nearly no documentation and the developer retired last month

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Clone a repository with SourceTree

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

### Running DB

Server: s???
---

