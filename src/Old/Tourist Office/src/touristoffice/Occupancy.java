package touristoffice;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Occupancy implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final int maxHistoryYears = 5;
    private static Set<Occupancy> occupancies = new HashSet<>();
	
    public int id; 
    public int noRooms;
    public int usedRooms;
    public int noBeds;
    public int usedBeds;
    
    public Occupancy(int id, int noRooms, int usedRooms, int noBeds, int usedBeds) {
    	Utils.checkIntNotNegative(noRooms, "Anzahl R�ume negativ");
		Utils.checkIntNotNegative(noBeds, "Anzahl Betten negativ");

    	this.id = id;
    	this.noRooms = noRooms;
    	this.usedRooms = usedRooms;
    	this.noBeds = noBeds;
    	this.usedBeds = usedBeds;
    }
    
    public static String constructFileName(int year, int month) {
    	//file name format is occYYYYMM with Y year and M month
    	return String.format("occ\\occ%04d%02d.dat", year, month);
    }
    
    public static void addOccupancy(Occupancy occ) {
    	occupancies.add(occ);
    }
    
    public static void clearOccupancies() {
    	occupancies.clear();
    }
}
