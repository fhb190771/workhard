package touristoffice;

import javax.swing.table.AbstractTableModel;

public class OccupancyTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	private static String columnNames[] = {"Kategorie","Zimmer","Zimmerauslastung","Betten","Bettenauslastung"};
	private int[][] data = new int[5][4];

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public int getRowCount() {
		return 5;
	}

	@Override
	public Object getValueAt(int row, int col) {
		if (col==0) {
			Category[] categories = Category.values();
			if (row == categories.length-1)
				return "Total";
			else if (row == categories.length-2)
				return categories[1]+" & "+categories[0];
			return categories[categories.length-1-row];
		}
		if (col%2 == 1)
			return data[row][col-1];
		else
			return data[row][col-2]==0 ? "0":String.format("%.1f", (double)(data[row][col-1])/data[row][col-2]*100);
	}

	public void addOccupancy(Hotel h, Occupancy occ) {
		int size = Category.values().length-1;
		int row = size - h.category.ordinal();
		if (row == size) row = size-1;
		data[row][0] += occ.noRooms;
		data[row][1] += occ.usedRooms;
		data[row][2] += occ.noBeds;
		data[row][3] += occ.usedBeds;
		data[size][0] += occ.noRooms;
		data[size][1] += occ.usedRooms;
		data[size][2] += occ.noBeds;
		data[size][3] += occ.usedBeds;
	}
	
	public void removeOccupancy(Hotel h, Occupancy occ) {
		int size = Category.values().length-1;
		int row = size - h.category.ordinal();
		if (row == size) row = size-1;
		data[row][0] -= occ.noRooms;
		data[row][1] -= occ.usedRooms;
		data[row][2] -= occ.noBeds;
		data[row][3] -= occ.usedBeds;
		data[size][0] -= occ.noRooms;
		data[size][1] -= occ.usedRooms;
		data[size][2] -= occ.noBeds;
		data[size][3] -= occ.usedBeds;
	}
	
	public void clearOccupancies() {
		int size = Category.values().length-1;
		for (int i=0; i<=size; ++i)
			for (int k=0; k<4; ++k)
				data[i][k]=0;
	}
}
