package gui.version10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class GUI {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShow();
			}
			
		});
	}

	private static void createAndShow() {
		final JFrame frm = new JFrame("Hello world");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setSize(300,200);
		frm.setLayout(null); //absolute Positionierung

		JMenuBar menuBar = new JMenuBar();
		JMenu menu1 = new JMenu("Men� 1");
		menuBar.add(menu1);
		JMenuItem item1 = new JMenuItem("11");
		item1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ad) {
				System.out.println("11 gew�hlt");
			}
		});
		menuBar.add(item1);
		JMenuItem item2 = new JMenuItem("21");
		menu1.add(item2);
		item2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ad) {
				System.out.println("21 gew�hlt");
			}
		});
		JMenuItem item3 = new JMenuItem("22");
		menu1.add(item3);
		item3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ad) {
				System.out.println("22 gew�hlt");
			}
		});
		frm.setJMenuBar(menuBar);

		frm.setVisible(true);
	}
}