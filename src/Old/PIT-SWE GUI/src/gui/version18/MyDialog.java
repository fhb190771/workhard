package gui.version18;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MyDialog {
	private Header ret;
	
	public Header doExternalDialog(final JFrame frm) {
		try {
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					createAndShow(frm);
				}
			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}	
		return ret;
	}
	
	public Header doDialog(JFrame frm) {
		createAndShow(frm);
		return ret;
	}
	
	public void createAndShow(JFrame frm) {
		
		final JDialog dlg = new JDialog(frm, "Eingabe", true);
		dlg.setLayout(null);
		
		Insets insets = dlg.getContentPane().getInsets();
		int xpos = insets.left+5;
		int ypos = insets.top+5;
		int gap = 2;

		ButtonGroup sex = new ButtonGroup();
		final JRadioButton btnM = new JRadioButton("m�nnlich");
		Dimension dim = btnM.getPreferredSize();
		btnM.setBounds(xpos,ypos,dim.width,dim.height);
		int yTmp=ypos+dim.height+2*gap;
		int xTmp=xpos+dim.width+7*gap;
		ypos+=dim.height;
		dlg.add(btnM);
		sex.add(btnM);
		final JRadioButton btnF = new JRadioButton("weiblich");
		btnF.setBounds(xpos,ypos,dim.width,dim.height);
		ypos += dim.height;
		dlg.add(btnF);
		sex.add(btnF);
		
		JLabel lblTitle = new JLabel("Titel:");
		dim = lblTitle.getPreferredSize();
		lblTitle.setBounds(xTmp,yTmp,dim.width,dim.height);
		xTmp += dim.width+gap;
		dlg.add(lblTitle);
		
		final JComboBox<Titel> title = new JComboBox<Titel>(Titel.values());
		dim = title.getPreferredSize();
		title.setBounds(xTmp,yTmp,dim.width,dim.height);
		xTmp += dim.width+60;
		dlg.add(title);

		JLabel lblFirstName = new JLabel("Vorname");
		dim = lblFirstName.getPreferredSize();
		lblFirstName.setBounds(xpos, ypos, dim.width, dim.height);
		dlg.add(lblFirstName);
		ypos += dim.height;
		final JTextField firstName = new JTextField(40);
		dim = firstName.getPreferredSize();
		firstName.setBounds(xpos, ypos, dim.width, dim.height);
		dlg.add(firstName);
		ypos += dim.height+gap;
		JLabel lblSecondName = new JLabel("Zuname");
		dim = lblSecondName.getPreferredSize();
		lblSecondName.setBounds(xpos, ypos, dim.width, dim.height);
		dlg.add(lblSecondName);
		ypos += dim.height;
		final JTextField secondName = new JTextField(40);
		dim = secondName.getPreferredSize();
		secondName.setBounds(xpos, ypos, dim.width, dim.height);
		dlg.add(secondName);
		ypos += dim.height+5*gap;
		
		JButton btnCancel = new JButton("Abbrechen");
		dim=btnCancel.getPreferredSize();
		btnCancel.setBounds(xpos,ypos,dim.width,dim.height);
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ad) {
				ret=null;
				dlg.dispose();
			}
		});
		dlg.add(btnCancel);
		xTmp = xpos+dim.width+gap;

		JButton btnOK = new JButton("OK");
		btnOK.setBounds(xTmp,ypos,dim.width,dim.height);
		btnOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ad) {
				String salutation="";
				if (btnM.isSelected()) salutation+="Sehr geehrter Herr ";
				if (btnF.isSelected()) salutation+="Sehr geehrte Frau ";
				salutation+=title.getSelectedItem().toString();
				ret=new Header(salutation,firstName.getText(),secondName.getText());
				dlg.dispose();
			}
		});	
		dlg.add(btnOK);
		
		dlg.setSize(500,210);
		dlg.setVisible(true); // Hier wird der Programmablauf unterbrochen
		
		//Hier geht es erst weiter, wenn der Dialog wieder unsichtbar/geschlossen wird.
	}
}
