package gui.version20;

public class Header {
	String salutation;
	String firstName;
	String secondName;
	
	public Header(String salutation, String firstName, String secondName) {
		this.salutation=salutation;
		this.firstName=firstName;
		this.secondName=secondName;
	}
	
	public String getSalutation() {
		return salutation;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return secondName;
	}
}
