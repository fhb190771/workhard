package gui.version17;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class GUI {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShow();
			}
			
		});
	}

	private static void createAndShow() {
		final JFrame frm = new JFrame("Calculator");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setSize(500,550);
		frm.setLayout(null); //absolute Positionierung

		Insets insets = frm.getContentPane().getInsets();
		int xpos = insets.left+5;
		int ypos = insets.top+5;
		int gap = 2;
		
		JLabel label1 = new JLabel("Eingabe 1:");
		frm.add(label1); 
		Dimension dim = label1.getPreferredSize();
		label1.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		
		final JTextField tf1 = new JTextField(20);
		frm.add(tf1);
		dim = tf1.getPreferredSize();
		tf1.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;

		JLabel label2 = new JLabel("Eingabe 2:");
		frm.add(label2); 
		dim = label2.getPreferredSize();
		label2.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		
		final JTextField tf2 = new JTextField(20);
		frm.add(tf2);
		dim = tf2.getPreferredSize();
		tf2.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;

		ypos+=10*gap;
		JLabel label3 = new JLabel("Ergebnis:");
		frm.add(label3); 
		dim = label3.getPreferredSize();
		label3.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;

		final JTextField res = new JTextField(20);
		frm.add(res);
		dim = res.getPreferredSize();
		res.setBounds(xpos,ypos,dim.width,dim.height);
		res.setFocusable(false); //kann nicht zur Eingabe ausgew�hlt werden
		ypos+=dim.height+gap;
		
		ypos+=dim.height+5*gap;
		JButton add = new JButton("Addieren");
		JButton sub = new JButton("Subtrahieren");
		frm.add(add);
		dim = sub.getPreferredSize();
		add.setBounds(xpos,ypos,dim.width,dim.height);
		frm.add(sub);
		sub.setBounds(xpos+dim.width+gap,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		ActionListener alBinary = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				try {
					double i1=Double.parseDouble(tf1.getText());
					double i2=Double.parseDouble(tf2.getText());
					if (ae.getActionCommand().equals("Addieren")) {
						res.setText(""+(i1+i2));
					}
					else {
						res.setText(""+(i1-i2));
					}
				}
				catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(frm,"Bitte nur Zahlen eingeben!");
				}
			}
			
		};
		add.addActionListener(alBinary);
		sub.addActionListener(alBinary);
		
		
		ypos+=dim.height+5*gap;
		final JCheckBox inv = new JCheckBox("Inv");
		frm.getContentPane().add(inv);
		dim = inv.getPreferredSize();
		inv.setBounds(xpos,ypos,dim.width,dim.height);
		int xposTmp=xpos+dim.width+2*gap;

		
		final JRadioButton deg = new JRadioButton("deg");
		final JRadioButton rad = new JRadioButton("rad");
		final ButtonGroup winkel = new ButtonGroup();
		winkel.add(deg);
		winkel.add(rad);
		deg.setSelected(true);
		frm.add(deg);
		frm.add(rad);
		dim = deg.getPreferredSize();
		deg.setBounds(xposTmp,ypos,dim.width,dim.height);
		rad.setBounds(xposTmp+gap+dim.width,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;

		
		ypos+=dim.height+5*gap;
		JButton sqr = new JButton("Quadrieren");
		frm.add(sqr);
		dim = sqr.getPreferredSize();
		sqr.setBounds(xpos,ypos,dim.width,dim.height);
		xposTmp=xpos+dim.width+gap;
		JButton sin = new JButton("Sinus");
		frm.add(sin);
		sin.setBounds(xposTmp,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		
		ActionListener alUnary = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				try {
					double i1=Double.parseDouble(tf1.getText());
					switch (ae.getActionCommand()) {
					case "Quadrieren":
						if (inv.isSelected())
							res.setText(""+Math.sqrt(i1));
						else
						    res.setText(""+(i1*i1));
						break;
					case "Sinus":
						double tmp=i1;
						if (inv.isSelected()) {
							tmp=Math.asin(tmp);
							if (deg.isSelected())
								tmp=tmp*180/Math.PI;
						}
						else {
							if (deg.isSelected())
								tmp=tmp/180*Math.PI;
							tmp=Math.sin(tmp);
						}
						res.setText(""+tmp);
						break;
						
					}
				}
				catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(frm,"Bitte nur Zahlen eingeben!");
				}
				
			}
			
		};
		sqr.addActionListener(alUnary);
		sin.addActionListener(alUnary);
		
		ypos+=dim.height+10*gap;
		JLabel labelMsg = new JLabel("Ihre Nachricht:");
		frm.add(labelMsg); 
		dim = labelMsg.getPreferredSize();
		labelMsg.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		JTextArea msg = new JTextArea(5,40);
		JScrollPane scroll = new JScrollPane(msg);
		dim = scroll.getPreferredSize();
		scroll.setBounds(xpos,ypos,dim.width,dim.height);
		frm.add(scroll);
		ypos+=dim.height+gap;
		JButton send = new JButton("Senden");
		dim = send.getPreferredSize();
		send.setBounds(xpos,ypos,scroll.getWidth(),dim.height);
		frm.add(send);
		
		JLabel label4 = new JLabel("Speicher:");
		dim = label4.getPreferredSize();
		xposTmp = res.getX()+res.getWidth()+15*gap;
		int yposTmp = label3.getY();
		label4.setBounds(xposTmp,yposTmp,dim.width,dim.height);
		frm.add(label4);
		yposTmp += dim.height+gap;
		
		final DefaultListModel<String> memoryModel = new DefaultListModel<String>();
		try {
			FileInputStream file = new FileInputStream("memory.sav");
			ObjectInputStream os = new ObjectInputStream(file);
			Object strings[] = (Object[])os.readObject();
			os.close();
			file.close();
			for (Object s : strings)
				memoryModel.addElement((String)s);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final JList<String> memory = new JList<String>(memoryModel);
		JScrollPane scrollMemory = new JScrollPane(memory);
		scrollMemory.setBounds(xposTmp, yposTmp, 187, sqr.getY()-gap-yposTmp);
		frm.add(scrollMemory);
		
		JButton store = new JButton("Speichern");
		dim = store.getPreferredSize();
		store.setBounds(xposTmp, sqr.getY(), dim.width, dim.height);
		store.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				String resTxt=res.getText();
				if (resTxt!=null && !resTxt.equals(""))
					memoryModel.addElement(resTxt);
			}
		});
		frm.add(store);
		final JButton recall = new JButton("Laden");
		recall.setBounds(xposTmp+dim.width+gap, sqr.getY(), dim.width, dim.height);
		recall.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				tf1.setText(memory.getSelectedValue());
			}
		});
		recall.setEnabled(false);
		frm.add(recall);
		final JButton remove = new JButton("L�schen");
		remove.setBounds(xposTmp, sqr.getY()+dim.height+gap, dim.width, dim.height);
		remove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				int del[] = memory.getSelectedIndices();
				for (int i=del.length-1; i>=0; --i) {
					memoryModel.remove(del[i]);
				}
			}
		});
		remove.setEnabled(false);
		frm.add(remove);
		final JButton mean = new JButton("Mittel");
		mean.setBounds(xposTmp+dim.width+gap, sqr.getY()+dim.height+gap, dim.width, dim.height);
		mean.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (memory.getSelectedIndices().length==0) return;
				double sum=0;
				for (String entry: memory.getSelectedValuesList())
					try {
						sum+=Double.parseDouble(entry);
					}
					catch(NumberFormatException e) {
						//ignore
					}
				res.setText(""+sum/memory.getSelectedValuesList().size());
			}
		});
		mean.setEnabled(false);
		frm.add(mean);
		memory.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				int noSelected = memory.getSelectedIndices().length;
				recall.setEnabled(noSelected==1);
				remove.setEnabled(noSelected>0);
				mean.setEnabled(noSelected>1);
			}
			
		});
		
		frm.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				try {
					FileOutputStream file = new FileOutputStream("memory.sav");
					ObjectOutputStream os = new ObjectOutputStream(file);
					os.writeObject(memoryModel.toArray());
					os.close();
					file.close();
					System.exit(0);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		frm.setVisible(true);
	}
}


