package gui.version8;

import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class GUI {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShow();
			}
			
		});
	}

	private static void createAndShow() {
		JFrame frm = new JFrame("Hello world");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setSize(300,200);
		frm.setLayout(null); //absolute Positionierung

		Insets insets = frm.getContentPane().getInsets();
		int xpos = insets.left+5;
		int ypos = insets.top+5;
		int gap = 2;
		
		JLabel label = new JLabel("Name:");
		frm.add(label); 
		Dimension dim = label.getPreferredSize();
		label.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		
		JTextField tf = new JTextField("Hans Huber", 20);
		frm.add(tf);
		dim = tf.getPreferredSize();
		tf.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;

		frm.setVisible(true);
	}
}