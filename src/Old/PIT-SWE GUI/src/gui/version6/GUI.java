package gui.version6;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class GUI {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShow();
			}
			
		});
	}

	private static void createAndShow() {
		JFrame frm = new JFrame("Hello world");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setSize(300,200);

		JLabel label = new JLabel("Eine einfache Beschriftung");
		frm.add(label); //Position des Labels wird durch default LayoutManager (BorderLayout) festgelegt

		frm.setVisible(true);
	}
}