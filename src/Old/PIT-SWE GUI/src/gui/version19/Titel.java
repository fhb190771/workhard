package gui.version19;

public enum Titel {
	NONE(""), DR("Dr."), DI("Dipl-Ing."), MAG("Mag.");

	private String txt;
	
	private Titel(String txt) {
		this.txt=txt;
	}
	
	@Override
	public String toString() {
		return txt;
	}
}
