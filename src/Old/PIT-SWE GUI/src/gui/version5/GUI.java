package gui.version5;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class GUI {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShow();
			}
			
		});
	}

	private static void createAndShow() {
		JFrame frm = new JFrame("Hello world");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setSize(300,200);
		frm.setVisible(true);
		JOptionPane.showMessageDialog(frm, "Jetzt geht's los!");
	}
}