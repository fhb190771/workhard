package gui.version11;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class GUI {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShow();
			}
			
		});
	}

	private static void createAndShow() {
		final JFrame frm = new JFrame("Calculator");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setSize(350,400);
		frm.setLayout(null); //absolute Positionierung

		Insets insets = frm.getContentPane().getInsets();
		int xpos = insets.left+5;
		int ypos = insets.top+5;
		int gap = 2;
		
		JLabel label1 = new JLabel("Eingabe 1:");
		frm.add(label1); 
		Dimension dim = label1.getPreferredSize();
		label1.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		
		final JTextField tf1 = new JTextField(20);
		frm.add(tf1);
		dim = tf1.getPreferredSize();
		tf1.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;

		JLabel label2 = new JLabel("Eingabe 2:");
		frm.add(label2); 
		dim = label2.getPreferredSize();
		label2.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		
		final JTextField tf2 = new JTextField(20);
		frm.add(tf2);
		dim = tf2.getPreferredSize();
		tf2.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;

		ypos+=10*gap;
		JLabel label3 = new JLabel("Ergebnis:");
		frm.add(label3); 
		dim = label3.getPreferredSize();
		label3.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;

		final JTextField res = new JTextField(20);
		frm.add(res);
		dim = res.getPreferredSize();
		res.setBounds(xpos,ypos,dim.width,dim.height);
		res.setFocusable(false); //kann nicht zur Eingabe ausgewählt werden
		ypos+=dim.height+gap;
		
		ypos+=dim.height+5*gap;
		JButton add = new JButton("Addieren");
		JButton sub = new JButton("Subtrahieren");
		frm.add(add);
		dim = sub.getPreferredSize();
		add.setBounds(xpos,ypos,dim.width,dim.height);
		frm.add(sub);
		sub.setBounds(xpos+dim.width+gap,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		ActionListener alBinary = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				try {
					double i1=Double.parseDouble(tf1.getText());
					double i2=Double.parseDouble(tf2.getText());
					if (ae.getActionCommand().equals("Addieren")) {
						res.setText(""+(i1+i2));
					}
					else {
						res.setText(""+(i1-i2));
					}
				}
				catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(frm,"Bitte nur Zahlen eingeben!");
				}
			}
			
		};
		add.addActionListener(alBinary);
		sub.addActionListener(alBinary);
		
		
		ypos+=dim.height+5*gap;
		final JCheckBox inv = new JCheckBox("Inv");
		frm.add(inv);
		dim = inv.getPreferredSize();
		inv.setBounds(xpos,ypos,dim.width,dim.height);
		int xposTmp=xpos+dim.width+2*gap;

		
		final JRadioButton deg = new JRadioButton("deg");
		final JRadioButton rad = new JRadioButton("rad");
		final ButtonGroup winkel = new ButtonGroup();
		winkel.add(deg);
		winkel.add(rad);
		deg.setSelected(true);
		frm.add(deg);
		frm.add(rad);
		dim = deg.getPreferredSize();
		deg.setBounds(xposTmp,ypos,dim.width,dim.height);
		rad.setBounds(xposTmp+gap+dim.width,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;

		
		ypos+=dim.height+5*gap;
		JButton sqr = new JButton("Quadrieren");
		frm.add(sqr);
		dim = sqr.getPreferredSize();
		sqr.setBounds(xpos,ypos,dim.width,dim.height);
		xposTmp=xpos+dim.width+gap;
		JButton sin = new JButton("Sinus");
		frm.add(sin);
		sin.setBounds(xposTmp,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		
		ActionListener alUnary = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				try {
					double i1=Double.parseDouble(tf1.getText());
					switch (ae.getActionCommand()) {
					case "Quadrieren":
						if (inv.isSelected())
							res.setText(""+Math.sqrt(i1));
						else
						    res.setText(""+(i1*i1));
						break;
					case "Sinus":
						double tmp=i1;
						if (inv.isSelected()) {
							tmp=Math.asin(tmp);
							if (deg.isSelected())
								tmp=tmp*180/Math.PI;
						}
						else {
							if (deg.isSelected())
								tmp=tmp/180*Math.PI;
							tmp=Math.sin(tmp);
						}
						res.setText(""+tmp);
						break;
						
					}
				}
				catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(frm,"Bitte nur Zahlen eingeben!");
				}
				
			}
			
		};
		sqr.addActionListener(alUnary);
		sin.addActionListener(alUnary);
		
		frm.setVisible(true);
	}
}