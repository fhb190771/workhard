package gui.version9;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class GUI {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShow();
			}
			
		});
	}

	private static void createAndShow() {
		final JFrame frm = new JFrame("Hello world");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setSize(300,200);
		frm.setLayout(null); //absolute Positionierung

		Insets insets = frm.getContentPane().getInsets();
		int xpos = insets.left+5;
		int ypos = insets.top+5;
		int gap = 2;
		
		JLabel label = new JLabel("Name:");
		frm.add(label); 
		Dimension dim = label.getPreferredSize();
		label.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		
		final JTextField tf = new JTextField("Hans Huber", 20);
		frm.add(tf);
		dim = tf.getPreferredSize();
		tf.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		
		JButton btn = new JButton("OK");
		frm.add(btn);
		dim = btn.getPreferredSize();
		ypos+=gap;
		btn.setBounds(xpos,ypos,dim.width,dim.height);
		ypos+=dim.height+gap;
		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent ae) {
				if (tf.getText().equals("Hans Huber"))
					JOptionPane.showMessageDialog(frm, "Sie haben ja gar nichts ver�ndert");
			}
		});

		frm.setVisible(true);
	}
}