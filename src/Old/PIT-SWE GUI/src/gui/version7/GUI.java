package gui.version7;

import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

public class GUI {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				createAndShow();
			}
			
		});
	}

	private static void createAndShow() {
		JFrame frm = new JFrame("Hello world");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frm.setSize(300,200);
		frm.setLayout(null); //absolute Positionierung

		Insets insets = frm.getContentPane().getInsets();
		
		JLabel label = new JLabel("Eine einfache Beschriftung");
		frm.add(label); 
		Dimension dim = label.getPreferredSize();
		label.setBounds(insets.left,insets.top,dim.width,dim.height);

		frm.setVisible(true);
	}
}