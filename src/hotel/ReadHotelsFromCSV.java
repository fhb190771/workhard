package hotel;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import occupancy.Occupancy;

public class ReadHotelsFromCSV {

	public static void main(String[] args) throws IOException {

//		List<Hotel> hotels = readHotelsFromCSV("C:\\Users\\Daniel\\Desktop\\MOCK_DATA.csv");

		
		List<Hotel> hotelList = readHotelsFromCSV("C:\\Users\\vbala\\git\\FührungsStrang\\src\\hotel\\MOCK_Hotel.csv");

		
		for (Hotel h : hotelList) {
			System.out.println(h);
		}

	}

	public static List<Hotel> readHotelsFromCSV(String filename) {

		List<Hotel> hotels = new ArrayList<>();
		Path pathToFile = Paths.get(filename);

		try (BufferedReader br = Files.newBufferedReader(pathToFile)) {

			String line = br.readLine();

			while (line != null) {

				String[] attributes = line.split(",");

				Hotel hotel = createHotel(attributes);

				hotels.add(hotel);

				line = br.readLine();
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return hotels;

	}
	

	private static Hotel createHotel(String[] metadata) {
													
		String category = metadata[0];		//FIXME: how to do with Enum?
		String name = metadata[1];
		String owner = metadata[2];
		String contact = metadata[3];
		String adress = metadata[4];
		String city = metadata[5];
		String cityCode = metadata[6];
		String phone = metadata[7];
		int rooms = Integer.parseInt(metadata[8]);
		int beds = Integer.parseInt(metadata[9]);

		return new Hotel(category, name, owner, contact, adress, city, cityCode, phone, rooms, beds);
	}
	

}
