package hotel;

import javax.persistence.Column;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

// TODO setters with input verification



@Entity
@Table(name = "hotel")
public class Hotel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hotel_ID")
	private int hotel_ID;
	@Column(name = "hotel_category")
	private String hotel_category;
	@Column(name = "hotel_name")
	private String hotel_name;
	@Column(name = "owner_name")
	private String owner_name;
	@Column(name = "hotel_contact")
	private String hotel_contact;
	@Column(name = "hotel_address")
	private String hotel_address;
	@Column(name = "hotel_addressNumber")
	private String hotel_addressNumber;
	@Column(name = "hotel_city")
	private String hotel_city;
	@Column(name = "hotel_cityCode")
	private String hotel_cityCode;
	@Column(name = "hotel_phone")
	private String hotel_phone;
	@Column(name = "hotel_rooms")
	private int hotel_rooms;
	@Column(name = "hotel_beds")
	private int hotel_beds;

	public Hotel(String category, String name, String owner_name, String contact, String address, String addressNumber, String city,
			String cityCode, String phone, int rooms, int beds) {

		this.hotel_category = category;
		this.hotel_name = name;
		this.owner_name = owner_name;
		this.hotel_contact = contact;
		this.hotel_address = address;
		this.hotel_addressNumber = addressNumber;
		this.hotel_city = city;
		this.hotel_cityCode = cityCode;
		this.hotel_phone = phone;
		this.hotel_rooms = rooms;
		this.hotel_beds = beds;
	}

	public Hotel() {
	}

	// **********Gettys&Settys************

	public String getHotel_category() {
		return hotel_category;
	}

	public void setHotel_category(String hotel_category) {
		this.hotel_category = hotel_category;
	}

	public String getHotel_name() {
		return hotel_name;
	}

	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}

	public String getOwner_name() {
		return owner_name;
	}

	public void setOwner_name(String owner_name) {
		this.owner_name = owner_name;
	}

	public String getHotel_contact() {
		return hotel_contact;
	}

	public void setHotel_contact(String hotel_contact) {
		this.hotel_contact = hotel_contact;
	}

	public String getHotel_address() {
		return hotel_address;
	}

	public void setHotel_address(String hotel_address) {
		this.hotel_address = hotel_address;
	}
	
	public String getHotel_addressNumber() {
		return hotel_addressNumber;
	}

	public void setHotel_addressNumber(String hotel_addressNumber) {
		this.hotel_addressNumber = hotel_addressNumber;
	}
	

	public String getHotel_city() {
		return hotel_city;
	}

	public void setHotel_city(String hotel_city) {
		this.hotel_city = hotel_city;
	}

	public String getHotel_cityCode() {
		return hotel_cityCode;
	}

	public void setHotel_cityCode(String hotel_cityCode) {
		this.hotel_cityCode = hotel_cityCode;
	}

	public String getHotel_phone() {
		return hotel_phone;
	}

	public void setHotel_phone(String hotel_phone) {
		this.hotel_phone = hotel_phone;
	}

	public int getHotel_rooms() {
		return hotel_rooms;
	}

	public void setHotel_rooms(int hotel_rooms) {
		this.hotel_rooms = hotel_rooms;
	}

	public int getHotel_beds() {
		return hotel_beds;
	}

	public void setHotel_beds(int hotel_beds) {
		this.hotel_beds = hotel_beds;
	}

	public int getHotel_ID() {
		return hotel_ID;
	}

	@Override
	public String toString() {
		return "Hotel [hotel_ID=" + hotel_ID + ", hotel_category=" + hotel_category + ", hotel_name=" + hotel_name
				+ ", owner_name=" + owner_name + ", hotel_contact=" + hotel_contact + ", hotel_address=" + hotel_address
				+ ", hotel_addressNumber=" + hotel_addressNumber + ", hotel_city=" + hotel_city + ", hotel_cityCode="
				+ hotel_cityCode + ", hotel_phone=" + hotel_phone + ", hotel_rooms=" + hotel_rooms + ", hotel_beds="
				+ hotel_beds + "]";
	}

}
