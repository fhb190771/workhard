package hotel;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import occupancy.Occupancy;
import owner.Owner;

public class SessionFactoryHotel {
	
	public static SessionFactory getSessionFactory() {
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Owner.class)
				.addAnnotatedClass(Hotel.class).addAnnotatedClass(Occupancy.class).buildSessionFactory();
		return factory;
	}

	public static Session getSession() {
		Session session = getSessionFactory().getCurrentSession();
		return session;
	}


}
