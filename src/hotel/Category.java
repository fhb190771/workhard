package hotel;

public enum Category {

	ONE("*"), TWO("**"), THREE("***"), FOUR("****"), FIVE("*****");
	
	private String stars;
	
	private Category(String stars) {
		this.stars = stars;
	}
	
	@Override
	public String toString() {
		return stars;
	}
}
