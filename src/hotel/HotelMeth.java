package hotel;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import occupancy.OccupancyMeth;
import owner.Owner;
import owner.OwnerMeth;

public class HotelMeth {

	public static void main(String[] args) {

		createHotel("5", "Kakerlakenhotel", "viky", "vika@mail.com", "irgendwo im nirgend wo", "12", "Wien", "12345",
				"019995554448", 2, 2);
//		updateHotel("4", "Känguruhotel", "viky", "vika@mail.com", "irgendwo im nirgend wo", "12", "Wien", "12345",
//				"019995554448", 2, 2);
//		deleteHotel("viky");
//		for (Occupancy occupancy : getHotelByID(1).getHotel_occupancies()) {
//			System.out.println(occupancy);
//		}

	}

	public static Hotel getHotelByID(int hotel_ID) {

		Session session = SessionFactoryHotel.getSession();

		session.beginTransaction();
		Hotel hotel = session.get(Hotel.class, hotel_ID);

		session.close();

		return hotel;
	}

	public static Hotel getHotelByOwner(String owner_name) {

		Session session = SessionFactoryHotel.getSession();

		session.beginTransaction();

		Owner owner = session.get(Owner.class, owner_name);
		System.out.println(owner);
		Hotel hotel = owner.getHotel();

		session.close();

		return hotel;
	}

	public static boolean checkIfHotelExists(String owner_name) {

		Session session = SessionFactoryHotel.getSession();

		session.beginTransaction();

		Criteria criteria = session.createCriteria(Hotel.class).add(Restrictions.like("owner_name", owner_name));

		if (criteria.list().isEmpty()) {
			session.close();
			return false;
		}
		session.close();
		return true;
	}

	public static boolean createHotel(String hotel_category, String hotel_name, String owner_name, String hotel_contact,
			String hotel_address, String hotel_addressNumber, String hotel_city, String hotel_cityCode,
			String hotel_phone, int hotel_rooms, int hotel_beds) {

		Session session = SessionFactoryHotel.getSession();

		session.beginTransaction();

		boolean success = false;
		boolean hotelExists = checkIfHotelExists(owner_name);
		boolean ownerExists = OwnerMeth.checkIfOwnerExists(owner_name);
		Owner owner = OwnerMeth.getOwner(owner_name);

		if (hotelExists == false) {

			if (ownerExists == true) {

				if (owner.getHotel() == null) {

					Hotel newHotel = new Hotel(hotel_category, hotel_name, owner_name, hotel_contact, hotel_address,
							hotel_addressNumber, hotel_city, hotel_cityCode, hotel_phone, hotel_rooms, hotel_beds);
					owner.setHotel(newHotel);
					session.update(owner);
					session.save(newHotel);
					
					success = true;
				} else {
					System.out.println("Owner already owns a hotel.");
				}

			} else {
				System.out.println("Owner does not exist.");
			}

		} else {
			System.out.println("Hotel for this Owner already exists.");
		}

		session.getTransaction().commit();
		return success;

	}

	public static boolean updateHotel(String hotel_category, String hotel_name, String owner_name, String hotel_contact,
			String hotel_address, String hotel_addressNumber, String hotel_city, String hotel_cityCode,
			String hotel_phone, int hotel_rooms, int hotel_beds) {

		Session session = SessionFactoryHotel.getSession();

		session.beginTransaction();

		boolean success = false;

		boolean hotelExists = checkIfHotelExists(owner_name);
		boolean ownerExists = OwnerMeth.checkIfOwnerExists(owner_name);
		Owner owner = OwnerMeth.getOwner(owner_name);
		Hotel hotel = getHotelByOwner(owner_name);

		if (hotelExists == true) {

			if (ownerExists == true) {

				if (owner.getHotel() != null) {

					hotel.setHotel_category(hotel_category);
					hotel.setHotel_name(hotel_name);
					hotel.setHotel_contact(hotel_contact);
					hotel.setHotel_address(hotel_address);
					hotel.setHotel_addressNumber(hotel_addressNumber);
					hotel.setHotel_city(hotel_cityCode);
					hotel.setHotel_cityCode(hotel_cityCode);
					hotel.setHotel_phone(hotel_phone);
					hotel.setHotel_rooms(hotel_rooms);
					hotel.setHotel_beds(hotel_beds);
					session.update(hotel);

					success = true;

				} else {
					System.out.println("Owner does not own a hotel.");

				}

			} else {
				System.out.println("Owner does not exist.");

			}

		} else {
			System.out.println("Hotel does not exist.");

		}

		session.getTransaction().commit();
		return success;

	}

	public static boolean deleteHotel(String owner_name) {
		Session session = SessionFactoryHotel.getSession();

		session.beginTransaction();

		boolean success = false;
		boolean hotelExists = checkIfHotelExists(owner_name);
		boolean ownerExists = OwnerMeth.checkIfOwnerExists(owner_name);
		Owner owner = OwnerMeth.getOwner(owner_name);
		Hotel hotel = getHotelByOwner(owner_name);

		if (hotelExists == true) {

			if (ownerExists == true) {

				if (owner.getHotel() != null) {

					session.delete(hotel);
					owner.setHotel(null);
					session.update(owner);
					OccupancyMeth.deleteOccupancyByOwner(owner_name);
					success = true;

				} else {
					System.out.println("Owner does not own a hotel.");
				}

			} else {
				System.out.println("Owner does not exist.");
			}

		} else {
			System.out.println("Hotel does not exist.");
		}

		session.getTransaction().commit();
		return success;
	}

}
