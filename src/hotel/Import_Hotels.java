package hotel;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class Import_Hotels {
	
	
	public static void main(String[] args) {
		
		List<Hotel> hotelList = ReadHotelsFromCSV.readHotelsFromCSV("C:\\Users\\vbala\\OneDrive\\Desktop\\MOCK_DATA.csv");

		massImportHotels(hotelList);
		
	}
	
	public static void massImportHotels(List<Hotel> listToImport){
		
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Hotel.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();

		try {

			System.out.println("Begin transaction");
			session.beginTransaction();
			int county = 0;
			
			for (Hotel h : listToImport) {
				session.save(h);
				county++;
			}
			session.getTransaction().commit();
			System.out.println(county + " new items have been added to database");

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		
	}
}
