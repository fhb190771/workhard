package owner;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;

import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.OneToOne;
import javax.persistence.Table;

import hotel.Hotel;

@Entity
@Table(name = "owner")
public class Owner {

	@Id
	@Column(name = "owner_name")
	private String owner_name;

	@Column(name = "owner_pw")
	private String owner_pw;

	@Column(name = "owner_isAdmin")
	private boolean owner_isAdmin;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "hotel_ID")
	private Hotel hotel;

	// test

	public Owner(String owner_name, String owner_pw, boolean owner_isAdmin) {
		this.owner_name = owner_name;
		this.owner_pw = owner_pw;
		this.owner_isAdmin = owner_isAdmin;
	}

	// this is a comment

	// test

	public Owner() {
	}

	public String getOwner_name() {
		return owner_name;
	}

	public void setOwner_name(String owner_name) {
		this.owner_name = owner_name;
	}

	public String getOwner_pw() {
		return owner_pw;
	}

	public void setOwner_pw(String owner_pw) {
		this.owner_pw = owner_pw;
	}

	public boolean isOwner_isAdmin() {
		return owner_isAdmin;
	}

	public void setOwner_isAdmin(boolean owner_isAdmin) {
		this.owner_isAdmin = owner_isAdmin;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	@Override
	public String toString() {
		return "Owner [owner_name=" + owner_name + ", owner_pw=" + owner_pw + ", owner_isAdmin=" + owner_isAdmin
				+ ", hotel=" + hotel + "]";
	}

}
