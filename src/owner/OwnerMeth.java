package owner;

import org.hibernate.Session;

public class OwnerMeth {

	public static void main(String[] args) {

		createOwner("dannyboi", "qwer1234", true);
		//createOwner("viky", "12345", false);

	}

	public static Owner getOwner(String name) {

		Session session = SessionFactoryOwner.getSession();

		session.beginTransaction();
		Owner owner = session.get(Owner.class, name);

		session.close();

		return owner;
	}

	public static boolean checkIfOwnerExists(String name) {

		Owner owner = getOwner(name);
		if (owner == null) {
			return false;
		}
		return true;
	}

	public static String getPassword(String name) {

		Owner owner = getOwner(name);
		return owner.getOwner_pw();
	}

	public static boolean benutzerIsAdmin(String name) {

		Owner owner = getOwner(name);

		return owner.isOwner_isAdmin();
	}

	public static boolean createOwner(String name, String password, boolean isAdmin) {

		Session session = SessionFactoryOwner.getSession();

		session.beginTransaction();

		boolean success = false;
		boolean ownerExists = checkIfOwnerExists(name);

		if (ownerExists == false) {
			Owner newOwner = new Owner(name, password, isAdmin);
			session.save(newOwner);
			success = true;
		} else {
			System.out.println("Owner Name already taken.");
		}

		session.getTransaction().commit();
		return success;

	}
}