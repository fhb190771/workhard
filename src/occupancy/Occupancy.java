package occupancy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "occupancy")
public class Occupancy {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "occupancy_ID")
	private int occupancy_ID;
	@Column(name = "occupancy_month")
	private int occupancy_month;
	@Column(name = "occupancy_year")
	private int occupancy_year;
	@Column(name = "occupied_rooms")
	private int occupied_rooms;
	@Column(name = "occupied_beds")
	private int occupied_beds;
	@Column(name = "owner_name")
	private String owner_name;
	
	public Occupancy(int occupancy_month, int occupancy_year, int occupied_rooms, int occupied_beds, String owner_name) {
		this.occupancy_month = occupancy_month;
		this.occupancy_year = occupancy_year;
		this.occupied_rooms = occupied_rooms;
		this.occupied_beds = occupied_beds;
		this.owner_name = owner_name;
	}

	public Occupancy() {

	}

	public int getOccupancy_month() {
		return occupancy_month;
	}

	public void setOccupancy_month(int occupancy_month) {
		this.occupancy_month = occupancy_month;
	}

	public int getOccupancy_year() {
		return occupancy_year;
	}

	public void setOccupancy_year(int occupancy_year) {
		this.occupancy_year = occupancy_year;
	}

	public int getOccupied_rooms() {
		return occupied_rooms;
	}

	public void setOccupied_rooms(int occupied_rooms) {
		this.occupied_rooms = occupied_rooms;
	}

	public int getOccupied_beds() {
		return occupied_beds;
	}

	public void setOccupied_beds(int occupied_beds) {
		this.occupied_beds = occupied_beds;
	}

	public int getOccupancy_ID() {
		return occupancy_ID;
	}

	public String getOwner_name() {
		return owner_name;
	}

	public void setOwner_name(String owner_name) {
		this.owner_name = owner_name;
	}

	@Override
	public String toString() {
		return "Occupancy [occupancy_ID=" + occupancy_ID + ", occupancy_month=" + occupancy_month + ", occupancy_year="
				+ occupancy_year + ", occupied_rooms=" + occupied_rooms + ", occupied_beds=" + occupied_beds
				+ ", owner_name=" + owner_name + "]";
	}
}
