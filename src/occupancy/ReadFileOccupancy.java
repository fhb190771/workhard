package occupancy;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import hotel.Hotel;

public class ReadFileOccupancy {
	
	public static void main(String[] args) throws IOException {

	//	List<Occupancy> occupancies = readOccupanciesFromCSV("C:\\Users\\Daniel\\Desktop\\MOCK_DATA_2.csv");
		
		List<Occupancy> occupancyList = readOccupanciesFromCSV("/nta/src/databaseStuff/fiki/files/MOCK_DATA2.csv");
				
		
		

//		for (Occupancy o : occupancyList) {
//			System.out.println(o);
//		}
	}
	
	public static List<Occupancy> readOccupanciesFromCSV(String filename) {

		List<Occupancy> occupancies = new ArrayList<>();
		Path pathToFile = Paths.get(filename);

		try (BufferedReader br = Files.newBufferedReader(pathToFile)) {

			String line = br.readLine();

			while (line != null) {

				String[] attributes = line.split(",");

				Occupancy occupancy = createOccupancy(attributes);

				occupancies.add(occupancy);

				line = br.readLine();
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return occupancies;
		
	}
	
	
	private static Occupancy createOccupancy(String[] metadata) {

		int hotel_ID= Integer.parseInt(metadata[0]);
		int month =  Integer.parseInt(metadata[1]);
		int year =  Integer.parseInt(metadata[2]);
		int occRooms = Integer.parseInt(metadata[3]);
		int occBeds = Integer.parseInt(metadata[4]);

		return new Occupancy(hotel_ID, month,year,occRooms,occBeds);
	}
}
