package occupancy;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import hotel.HotelMeth;
import owner.OwnerMeth;

public class OccupancyMeth {

	public static void main(String[] args) {

		createOccupancy(11, 1991, 4, 14, "viky");
//		createOccupancy(12, 1991, 4, 14, "viky");
//		createOccupancy(1, 1991, 4, 14, "viky");
////		createOccupancy(1, 1991, 4, 14, "viky");
//
//		List<Occupancy> occus = getOccupancyByOwner("viky");
//
//		System.out.println(occus.toString());

//		updateOccupancy(11, 1990, 8, 20, "viky", 1);
//		deleteOccupancyByID("viky", 2);
//		deleteOccupancyByOwner("viky");
		
		System.out.println(getOccupancy("viky", 11, 1991));
		

	}
	
	public static Occupancy getOccupancy(String owner_name, int occupancy_month, int occupancy_year) {
		
		Session session = SessionFactoryOccupancy.getSession();
		
		session.beginTransaction();
		Occupancy occupancy = null;
		
		Criteria criteria = session.createCriteria(Occupancy.class)
				.add(Restrictions.eq("occupancy_month", occupancy_month))
				.add(Restrictions.eq("occupancy_year", occupancy_year))
				.add(Restrictions.like("owner_name", owner_name));
		
		occupancy = (Occupancy) criteria.uniqueResult();
		
		session.close();
		
		return occupancy;
	}

	public static Occupancy getOccupancyByID(int occupancy_ID) {

		Session session = SessionFactoryOccupancy.getSession();

		session.beginTransaction();
		Occupancy occupancy = session.get(Occupancy.class, occupancy_ID);

		session.close();

		return occupancy;
	}

	public static List<Occupancy> getOccupancyByOwner(String owner_name) {

		Session session = SessionFactoryOccupancy.getSession();

		session.beginTransaction();

		List<Occupancy> occupancies;

		Criteria criteria = session.createCriteria(Occupancy.class).add(Restrictions.eq("owner_name", owner_name));

		occupancies = criteria.list();

		session.close();

		return occupancies;
	}

	public static boolean checkIfOccupancyExists(String owner_name, int occupancy_month, int occupancy_year) {

		Session session = SessionFactoryOccupancy.getSession();

		session.beginTransaction();

		Criteria criteria = session.createCriteria(Occupancy.class)
				.add(Restrictions.eq("occupancy_month", occupancy_month))
				.add(Restrictions.eq("occupancy_year", occupancy_year))
				.add(Restrictions.like("owner_name", owner_name));

		if (criteria.list().isEmpty()) {
			session.close();
			return false;
		}
		session.close();
		return true;
	}

	public static boolean createOccupancy(int occupancy_month, int occupancy_year, int occupied_rooms, int occupied_beds,
			String owner_name) {

		Session session = SessionFactoryOccupancy.getSession();

		session.beginTransaction();

		boolean success = false;
		boolean occupancyExists = checkIfOccupancyExists(owner_name, occupancy_month, occupancy_year);
		boolean ownerExists = OwnerMeth.checkIfOwnerExists(owner_name);
		boolean hotelExists = HotelMeth.checkIfHotelExists(owner_name);

		if (occupancyExists == false) {

			if (hotelExists == true) {

				if (ownerExists == true) {

					Occupancy newOccupancy = new Occupancy(occupancy_month, occupancy_year, occupied_rooms,
							occupied_beds, owner_name);

					session.save(newOccupancy);
					success = true;

				} else {
					System.out.println("Owner does not exist.");
				}

			} else {
				System.out.println("Hotel does not exist.");
			}

		} else {
			System.out.println("Occupancy for this hotel, in this month and in this year, already exists.");
		}

		session.getTransaction().commit();
		return success;

	}

	public static boolean updateOccupancy(int occupancy_month, int occupancy_year, int occupied_rooms, int occupied_beds,
			String owner_name) {

		Session session = SessionFactoryOccupancy.getSession();

		session.beginTransaction();

		boolean success = false;
		Occupancy occupancy = getOccupancy(owner_name, occupancy_month, occupancy_year);
		boolean occupancyExists = checkIfOccupancyExists(owner_name, occupancy.getOccupancy_month(),
				occupancy.getOccupancy_year());
		boolean ownerExists = OwnerMeth.checkIfOwnerExists(owner_name);
		boolean hotelExists = HotelMeth.checkIfHotelExists(owner_name);

		if (occupancyExists == true) {

			if (hotelExists == true) {

				if (ownerExists == true) {

					occupancy.setOccupied_beds(occupied_beds);
					occupancy.setOccupied_rooms(occupied_rooms);
					session.update(occupancy);
					success = true;

				} else {
					System.out.println("Owner does not exist.");
				}

			} else {
				System.out.println("Hotel does not exist.");
			}

		} else {
			System.out.println("Occupancy does not exists.");
		}

		session.getTransaction().commit();
		return success;

	}

	public static boolean deleteOccupancy(String owner_name, int occupancy_month, int occupancy_year) {

		Session session = SessionFactoryOccupancy.getSession();

		session.beginTransaction();

		boolean success = false;
		Occupancy occupancy = getOccupancy(owner_name, occupancy_month, occupancy_year);
		boolean occupancyExists = checkIfOccupancyExists(owner_name, occupancy.getOccupancy_month(),
				occupancy.getOccupancy_year());
		boolean ownerExists = OwnerMeth.checkIfOwnerExists(owner_name);
		boolean hotelExists = HotelMeth.checkIfHotelExists(owner_name);

		if (occupancyExists == true) {

			if (hotelExists == true) {

				if (ownerExists == true) {

					session.delete(occupancy);
					success = true;

				} else {
					System.out.println("Owner does not exist.");
				}

			} else {
				System.out.println("Hotel does not exist.");
			}

		} else {
			System.out.println("Occupancy does not exists.");
		}

		session.getTransaction().commit();
		return success;

	}

	public static boolean deleteOccupancyByOwner(String owner_name) {

		Session session = SessionFactoryOccupancy.getSession();

		session.beginTransaction();

		boolean success = false;
		int count = 0;
		List<Occupancy> occupancies = getOccupancyByOwner(owner_name);

		for (Occupancy occupancy : occupancies) {

			boolean occupancyExists = checkIfOccupancyExists(owner_name, occupancy.getOccupancy_month(),
					occupancy.getOccupancy_year());
			boolean ownerExists = OwnerMeth.checkIfOwnerExists(owner_name);
			boolean hotelExists = HotelMeth.checkIfHotelExists(owner_name);

			if (occupancyExists == true) {

				if (hotelExists == true) {

					if (ownerExists == true) {

						session.delete(occupancy);
						count++;

					} else {
						System.out.println("Owner does not exist.");
					}

				} else {
					System.out.println("Hotel does not exist.");
				}

			} else {
				System.out.println("Occupancy does not exists.");
			}

		}
		
		if (count == occupancies.size()) {
			success = true;
		}
		
		session.getTransaction().commit();
		return success;

	}

}
