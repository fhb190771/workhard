USE master

IF EXISTS (
		SELECT *
		FROM sys.databases
		WHERE name = 'bio'
		)
	DROP DATABASE bio

CREATE DATABASE bio
GO

USE bio
GO

CREATE TABLE mitarbeiter(
	mitarbeiterID INT identity(1000,1) PRIMARY KEY,
	mitarbeiterVorname VARCHAR(20) NOT NULL,
	mitarbeiterNachname VARCHAR(20) NOT NULL,
	geburtsdatum DATE NOT NULL,
	eintrittsdatum DATE NOT NULL,
	austrittsdatum DATE,
	lohn MONEY
)

GO

INSERT INTO mitarbeiter VALUES ('Max', 'Muster', '09-09-1999', '10-10-2010',null,1234)
Select * from mitarbeiter

GO

CREATE TABLE apfel (
	apfelid INT identity(1, 1) PRIMARY KEY
	,apfelname VARCHAR(20),
	gewicht decimal(5,2)
	)
GO

INSERT INTO apfel
VALUES ('Granny Smith', 1.0)
	,('Golden Delicious', 2.0)
	,('Kronprinz Rudolf', 3.0)

SELECT *
FROM apfel
GO
create table kiste(
id int primary key,
bez varchar(20) unique
)

USE [master]
GO

CREATE LOGIN [user]
	WITH PASSWORD = N'user'
		,DEFAULT_DATABASE = [bio]
		,DEFAULT_LANGUAGE = [Deutsch]
		,CHECK_EXPIRATION = OFF
		,CHECK_POLICY = OFF
GO

ALTER SERVER ROLE [dbcreator] ADD MEMBER [user]
GO

USE [bio]
GO

CREATE USER [user]
FOR LOGIN [user]
GO

USE [bio]
GO

ALTER ROLE [db_owner] ADD MEMBER [user]
GO

-- check sql port and status
USE master
GO

xp_readerrorlog 0
	,1
	,N'Server is listening on'
GO

-- check with tcpview(application) if port is in use or/and status