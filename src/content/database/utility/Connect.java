package content.database.utility;


import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class Connect {
	
	private static final String url = "jdbc:sqlserver://";
	private static final String serverName = ServerName.getServerName();//"V0001303";
	private static final String portNumber = "1433";
	private static final String databaseName = "bio";
	private static final String userName = "user";
	private static final String password = "user";

	public static String getConnectionUrl() {

		return url + serverName + ":" + portNumber + ";databaseName=" + databaseName;
	}

	public static Connection getConnection() {

		Connection con = null;

		try {
			
			con = DriverManager.getConnection(getConnectionUrl(), userName, password);

			if (con != null) {
			//	System.out.println("Connection Successful!");
			}
		} catch (Exception e) {
			System.out.println("Fehler beim Aufbau der Verbindung zur DB: " + e.getLocalizedMessage());
		}
		return con;
	}

	public static void closeConnection(Connection con) {

		try {
			if (con != null) {
				con.close();
			}
			con = null;
		} catch (Exception e) {
			System.out.println("Fehler beim Beenden der Verbindung zur DB: " + e.getLocalizedMessage());
		}
	}

	public static boolean checkConnection() {

		Connection con = Connect.getConnection();
		if (con != null) {
			Connect.closeConnection(con);
			return true;
		}
		return false;
	}
	
	public static void displayDbProperties() {

		Connection con = null;
		DatabaseMetaData dm = null;
		ResultSet rs = null;

		try {
			con = Connect.getConnection();
			dm = con.getMetaData();
			System.out.println("Driver Information");
			System.out.println("\tDriver Name: " + dm.getDriverName());
			System.out.println("\tDriver Version: " + dm.getDriverVersion());
			System.out.println("Database Information ");
			System.out.println("\tDatabase Name: " + dm.getDatabaseProductName());
			System.out.println("\tDatabase Version: " + dm.getDatabaseProductVersion());
			System.out.println("Vorhandene Datenbanken:");
			rs = dm.getCatalogs();
			while (rs.next()) {
				System.out.println("\t" + rs.getString(1));
			}
			rs.close();
			rs = null;
		} catch (Exception e) {
			fehlerAusgabe(e);
		} finally {
			Connect.closeConnection(con);
		}
		dm = null;
	}

	private static void fehlerAusgabe(Exception e) {
		System.err.println(e.getLocalizedMessage());
		
	}
}
