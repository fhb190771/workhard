package content.database.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Test2Connect {

	public static void main(String[] args) {

		
			

		String jdbcUrl = "jdbc:sqlserver://localhost:1433;databaseName=bio";
		String user = "user";
		String pwd = "user";
		
		
		System.out.println("Connecting to database: " + jdbcUrl);
		
		try {
		Connection con = DriverManager.getConnection(jdbcUrl, user, pwd);
		
			
			System.out.println("OK");
		} catch (SQLException e) {
			System.out.println(e.getLocalizedMessage());
			System.out.println(e.getErrorCode());
			System.out.println("NOT OK");

		}
		
		
	}

}
