package content.database.utility;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ServerName {
	
	public static void main(String[] args) {
		System.out.println(getServerName());
	}

	public static String getServerName() {

		String computername = null;
		try {
			computername = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return e.getMessage();
		}
		return computername;

	}
	

}
