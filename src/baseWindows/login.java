package baseWindows;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import owner.OwnerMeth;
import run.Main_Main_main;

import java.awt.Color;
import java.awt.Button;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JPasswordField;

public class login extends JFrame {
	
	//TODO: User&Passwort&isAdmin mit db verbinden
	private String correctUser;
	private String correctPWD;
	private boolean isAdmin = false;
	private static int counter = 0;

	private JPanel contentPane_background;
	private JTextField textField_userName;
	private JPasswordField passwordField_password;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					login frame = new login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public login() {
		setTitle("Login");
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 860, 530);
		contentPane_background = new JPanel();
		contentPane_background.setBackground(Color.BLACK);
		contentPane_background.setBorder(new EmptyBorder(2, 2, 2, 2));
		setContentPane(contentPane_background);
		contentPane_background.setLayout(null);
		
		Button button_logIn = new Button("Log In");
		button_logIn.setForeground(new Color(248,178,127));
		button_logIn.setBackground(new Color(146, 15, 0));
		button_logIn.setBounds(451, 323, 334, 39);
		contentPane_background.add(button_logIn);
		
		textField_userName = new JTextField();
		textField_userName.setBackground(new Color(248,178,127));
		textField_userName.setBounds(450, 125, 330, 35);
		contentPane_background.add(textField_userName);
		
		JLabel lblNewLabel_userName = new JLabel("Benutzername");
		lblNewLabel_userName.setForeground(new Color(248,178,127));
		lblNewLabel_userName.setFont(new Font("Calibri Light", Font.BOLD, 20));
		lblNewLabel_userName.setBounds(450, 95, 330, 35);
		contentPane_background.add(lblNewLabel_userName);
		
		JLabel lblNewLabel_password = new JLabel("Passwort");
		lblNewLabel_password.setForeground(new Color(248,178,127));
		lblNewLabel_password.setFont(new Font("Calibri Light", Font.BOLD, 20));
		lblNewLabel_password.setBounds(450, 200, 330, 35);
		contentPane_background.add(lblNewLabel_password);
		
		passwordField_password = new JPasswordField();
		passwordField_password.setBackground(new Color(248,178,127));
		passwordField_password.setBounds(451, 230, 330, 35);
		contentPane_background.add(passwordField_password);
		
		JLabel lblNewLabel_image = new JLabel("");
		lblNewLabel_image.setIcon(new ImageIcon(login.class.getResource("/images/pexels-junior-teixeira-2047905 (Klein).jpg")));
		lblNewLabel_image.setBounds(58, 10, 324, 465);
		contentPane_background.add(lblNewLabel_image);
		
		JLabel lblNewLabel_message = new JLabel("");
		lblNewLabel_message.setForeground(Color.WHITE);
		lblNewLabel_message.setBounds(451, 377, 334, 27);
		contentPane_background.add(lblNewLabel_message);
		
		textField_userName.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate (CaretEvent e) {
				if(textField_userName.getText().trim().length() > 3) {
					button_logIn.setEnabled(true);
				}else {
					button_logIn.setEnabled(false);
				}
			}
		});
		
		button_logIn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String username = textField_userName.getText();
				
				if(username.isEmpty()) {
					JOptionPane.showMessageDialog(null, "Es wurde kein Benutzername eingegeben");
				}
				char pwdChar[] = passwordField_password.getPassword();
				String password = String.valueOf(pwdChar);
				if(password.isEmpty()) {
					JOptionPane.showMessageDialog(null, "Es wurde kein Passwort eingegeben");
				}
				if(OwnerMeth.checkIfOwnerExists(username) == true) {
					correctPWD = OwnerMeth.getPassword (username);
					isAdmin = OwnerMeth.benutzerIsAdmin(username);
					if (password.equals(correctPWD)) {	
						Main_Main_main.setLosername(username);
						Main_Main_main.setLoserOwner(OwnerMeth.getOwner(username));
						System.out.println("this is the active user " + Main_Main_main.getLosername());
						if(isAdmin == true) {
							new MainWindowAdminUser().setVisible(true);
							dispose();
						}else {
							new MainWindowHotelUser().setVisible(true);
							dispose();
						}
					}else {
						lblNewLabel_message.setText("Benutzername oder Passwort ist nicht korrekt");
						counter++;
					}	
				}
				if(counter == 3) {
					JOptionPane.showMessageDialog(null, "Fenster wird geschlossen, da 3 Versuche gescheitert sind");
					System.exit(1); //TODO: warum steht hier eine 1
				}
				
				
			}
		});
		
		
	}
}
