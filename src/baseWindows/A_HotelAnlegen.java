package baseWindows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JSeparator;
import java.awt.GridLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.LineBorder;

import hotel.HotelMeth;
import owner.OwnerMeth;
import run.Main_Main_main;
import werkzeugkasten.Utittity;

import javax.swing.UIManager;

public class A_HotelAnlegen extends JFrame {
		
		//TODO � erlauben
		

		private JPanel contentPane;
		private JTextField textField_Name;
		private JTextField textField_Street;
		private JTextField textField_Number;
		private JTextField textField_Citycode;
		private JTextField textField_City;
		private JTextField textField_Category;
		private JTextField textField_Contact;
		private JTextField textField_Phone;
		private JTextField textField_Rooms;
		private JTextField textField_Beds;
		private JTextField textField_Benutzername;

		/**
		 * Launch the application.
		 */
		public static void main(String[] args) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						A_HotelAnlegen frame = new A_HotelAnlegen();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

		/**
		 * Create the frame.
		 */
		public A_HotelAnlegen() {
			setResizable(false);
			setBackground(new Color(244,241,240));
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 800, 400);
			contentPane = new JPanel();
			contentPane.setBackground(new Color(236, 232, 228));
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			
			JLabel lblNewLabel_title = new JLabel("Hotel anlegen f\u00FCr");
			lblNewLabel_title.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_title.setForeground(new Color(98, 0, 55));
			lblNewLabel_title.setFont(new Font("Calibri", Font.BOLD, 20));
			
			JPanel panel = new JPanel();
			panel.setBorder(new LineBorder(new Color(126,110,98)));
			panel.setBackground(new Color(236, 232, 228));
			
			JButton button_Save = new JButton("Speichern");
			button_Save.setForeground(Color.BLACK);
			button_Save.setBackground(new Color(120,104,91));
			button_Save.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			JButton button_Stop = new JButton("Abbrechen");
			button_Stop.setForeground(Color.BLACK);
			button_Stop.setFont(new Font("Calibri", Font.PLAIN, 14));
			button_Stop.setBackground(new Color(120, 104, 91));
			
			JLabel lblNewLabel_forMessage = new JLabel("");
			lblNewLabel_forMessage.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
			lblNewLabel_forMessage.setForeground(new Color(164,26,62));
			
			JLabel lblNewLabel_Benutzername = new JLabel("Benutzername");
			lblNewLabel_Benutzername.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_Benutzername.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_Benutzername.setForeground(new Color(98, 0, 55));
			lblNewLabel_Benutzername.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_Benutzername = new JTextField();
			textField_Benutzername.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_Benutzername.setColumns(10);
			GroupLayout gl_contentPane = new GroupLayout(contentPane);
			gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGap(10)
								.addComponent(lblNewLabel_forMessage, GroupLayout.DEFAULT_SIZE, 379, Short.MAX_VALUE)
								.addGap(18)
								.addComponent(button_Stop, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(button_Save, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
								.addGap(125))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(panel, GroupLayout.PREFERRED_SIZE, 696, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(80, Short.MAX_VALUE))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGap(48)
								.addComponent(lblNewLabel_title, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(lblNewLabel_Benutzername, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(textField_Benutzername, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
								.addContainerGap())))
			);
			gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_contentPane.createSequentialGroup()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewLabel_title, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(3)
									.addComponent(lblNewLabel_Benutzername, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
									.addGap(3)))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGap(12)
								.addComponent(textField_Benutzername, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGap(18)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
									.addComponent(button_Save)
									.addComponent(button_Stop, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(lblNewLabel_forMessage, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addGap(40))
			);
			
			JLabel lblNewLabel_Name = new JLabel("Hotelname");
			lblNewLabel_Name.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_Name.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_Name.setForeground(new Color(98,0,55));
			lblNewLabel_Name.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_Name = new JTextField();
			lblNewLabel_Name.setLabelFor(textField_Name);
			textField_Name.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_Name.setColumns(10);
			
			JLabel lblNewLabel_Street = new JLabel("Stra\u00DFe");
			lblNewLabel_Street.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_Street.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_Street.setForeground(new Color(98, 0, 55));
			lblNewLabel_Street.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_Street = new JTextField();
			textField_Street.setForeground(Color.BLACK);
			textField_Street.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_Street.setColumns(10);
			
			JLabel lblNewLabel_Number = new JLabel("Hausnummer");
			lblNewLabel_Number.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_Number.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_Number.setForeground(new Color(98, 0, 55));
			lblNewLabel_Number.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_Number = new JTextField();
			textField_Number.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_Number.setColumns(10);
			
			JLabel lblNewLabel_Citycode = new JLabel("Postleitzahl");
			lblNewLabel_Citycode.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_Citycode.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_Citycode.setForeground(new Color(98, 0, 55));
			lblNewLabel_Citycode.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_Citycode = new JTextField();
			textField_Citycode.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_Citycode.setColumns(10);
			
			JLabel lblNewLabel_City = new JLabel("Stadt");
			lblNewLabel_City.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_City.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_City.setForeground(new Color(98, 0, 55));
			lblNewLabel_City.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_City = new JTextField();
			textField_City.setHorizontalAlignment(SwingConstants.LEFT);
			textField_City.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_City.setColumns(10);
			
			JLabel lblNewLabel_Category = new JLabel("Sterne");
			lblNewLabel_Category.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_Category.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_Category.setForeground(new Color(98, 0, 55));
			lblNewLabel_Category.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_Category = new JTextField();
			textField_Category.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_Category.setColumns(10);
			
			JLabel lblNewLabel_Contact = new JLabel("E-Mail");
			lblNewLabel_Contact.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_Contact.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_Contact.setForeground(new Color(98, 0, 55));
			lblNewLabel_Contact.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_Contact = new JTextField();
			textField_Contact.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_Contact.setColumns(10);
			
			JLabel lblNewLabel_Phone = new JLabel("Mobil");
			lblNewLabel_Phone.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_Phone.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_Phone.setForeground(new Color(98, 0, 55));
			lblNewLabel_Phone.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_Phone = new JTextField();
			textField_Phone.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_Phone.setColumns(10);
			
			JLabel lblNewLabel_Rooms = new JLabel("R\u00E4ume");
			lblNewLabel_Rooms.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_Rooms.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_Rooms.setForeground(new Color(98, 0, 55));
			lblNewLabel_Rooms.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_Rooms = new JTextField();
			textField_Rooms.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_Rooms.setColumns(10);
			
			JLabel lblNewLabel_Beds = new JLabel("Betten");
			lblNewLabel_Beds.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_Beds.setHorizontalAlignment(SwingConstants.RIGHT);
			lblNewLabel_Beds.setForeground(new Color(98, 0, 55));
			lblNewLabel_Beds.setFont(new Font("Calibri", Font.PLAIN, 14));
			
			textField_Beds = new JTextField();
			textField_Beds.setFont(new Font("Calibri", Font.PLAIN, 14));
			textField_Beds.setColumns(10);
			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
							.addGroup(gl_panel.createSequentialGroup()
								.addContainerGap()
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panel.createSequentialGroup()
										.addComponent(lblNewLabel_Name, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(textField_Name, GroupLayout.DEFAULT_SIZE, 643, Short.MAX_VALUE))
									.addGroup(gl_panel.createSequentialGroup()
										.addComponent(lblNewLabel_Street, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(textField_Street, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE)
										.addGap(18)
										.addComponent(lblNewLabel_Number, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(textField_Number, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE))
									.addGroup(gl_panel.createSequentialGroup()
										.addComponent(lblNewLabel_Citycode, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(textField_Citycode, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
										.addComponent(lblNewLabel_City, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
										.addGap(18)
										.addComponent(textField_City, GroupLayout.PREFERRED_SIZE, 421, GroupLayout.PREFERRED_SIZE))))
							.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
								.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
									.addComponent(lblNewLabel_Category, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
									.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel_Phone, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblNewLabel_Contact, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addGroup(gl_panel.createSequentialGroup()
										.addComponent(textField_Category, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED, 86, Short.MAX_VALUE)
										.addComponent(lblNewLabel_Rooms, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(textField_Rooms, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(lblNewLabel_Beds, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(textField_Beds, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE))
									.addComponent(textField_Contact, GroupLayout.DEFAULT_SIZE, 598, Short.MAX_VALUE)
									.addComponent(textField_Phone, GroupLayout.DEFAULT_SIZE, 598, Short.MAX_VALUE))))
						.addContainerGap())
			);
			gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblNewLabel_Name)
							.addComponent(textField_Name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(13)
								.addComponent(lblNewLabel_Street, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addComponent(textField_Number, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
									.addComponent(textField_Street, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(13)
								.addComponent(lblNewLabel_Number, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(3)
								.addComponent(lblNewLabel_Citycode, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
							.addComponent(textField_Citycode, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField_City, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_City, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(3)
								.addComponent(lblNewLabel_Category, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(3)
								.addComponent(lblNewLabel_Rooms, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(3)
								.addComponent(lblNewLabel_Beds, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
							.addComponent(textField_Beds, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
							.addComponent(textField_Category, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
							.addComponent(textField_Rooms, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblNewLabel_Contact, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
							.addComponent(textField_Contact, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblNewLabel_Phone, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
							.addComponent(textField_Phone, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(39, Short.MAX_VALUE))
			);
			panel.setLayout(gl_panel);
			contentPane.setLayout(gl_contentPane);
			
			button_Save.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					String user = textField_Benutzername.getText();
					if(OwnerMeth.checkIfOwnerExists(user) == false) {
						JOptionPane.showMessageDialog(lblNewLabel_title, "Dieser Benutzer existiert nicht!");
					}else {
						boolean correctInput = true;
						String nameinput = textField_Name.getText();
						if(nameinput.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde kein Hotelname eingegeben");
						}
						String streetinput = textField_Street.getText();
						if(streetinput.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde keine Stra�e eingegeben");
						}
						String numberinput = textField_Number.getText();
						if(numberinput.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde keine Hausnummer eingegeben");
						}
						String citycodeinput = textField_Citycode.getText();
						if(citycodeinput.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde keine Postleitzahl eingegeben");
						}
						String cityinput = textField_City.getText();
						if(cityinput.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde keine Stadt eingegeben");
						}
						String categoryinput = textField_Category.getText();
						if(categoryinput.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde keine Kategorie eingegeben");
						}
						String roomsinput = textField_Rooms.getText();
						if(roomsinput.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde keine Raumanzahl eingegeben");
						}
						String bedinput = textField_Beds.getText();
						if(bedinput.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde keine Bettanzahl eingeben");
						}
						String contactinput = textField_Contact.getText();
						if(contactinput.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde keine Mail eingeben");
						}
						String phoneinput = textField_Phone.getText();
						if(phoneinput.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde keine Mail eingeben");
						}
						
						if(Utittity.stringPruefer(nameinput, 50, 1, false, false, true) == false) { 
							lblNewLabel_forMessage.setText("Achtung!: Bitte geben Sie einen passenden Namen an");
							correctInput = false;
						}
						if(Utittity.stringPruefer(streetinput, 50, 1, false, false, true) == false){
							lblNewLabel_forMessage.setText("Achtung!: Bitte geben Sie einen passende Stra�e an");
							correctInput = false;
						}
						if(Utittity.nurziffern(numberinput) == false){
							lblNewLabel_forMessage.setText("Achtung!: Bitte geben Sie bei der Stra�e nur Ziffern an");
							correctInput = false;
						}
						if(Utittity.nurziffern(citycodeinput) == false){
							lblNewLabel_forMessage.setText("Achtung!: Bitte geben Sie bei der Postleitzahl nur Ziffern an");
							correctInput = false;
						}
						if(Utittity.stringPruefer(cityinput, 50, 1, false, false, true) == false){
							lblNewLabel_forMessage.setText("Achtung!: Bitte geben Sie einen passende Stadt an");
							correctInput = false;
						}
						if(Utittity.checkcategory(categoryinput)==false) {
							lblNewLabel_forMessage.setText("Achtung!: Bitte geben Sie einen passende Kategorie an (*, **, ***, ****, *****");
							correctInput = false;
						}
						if(Utittity.nurziffern(roomsinput) == false) {
							lblNewLabel_forMessage.setText("Achtung!: Bitte geben Sie einen passende Raumanzahl an");
							correctInput = false;
						}
						if(Utittity.nurziffern(bedinput) == false) {
							lblNewLabel_forMessage.setText("Achtung!: Bitte geben Sie einen passende Bettenanzahl an");
							correctInput = false;
						}
						if(Utittity.isValidMail(contactinput) == false) {
							lblNewLabel_forMessage.setText("Achtung!: Bitte geben Sie einen passende Mail an");
							correctInput = false;
						}
						if(Utittity.nurziffern(phoneinput) == false) {
							lblNewLabel_forMessage.setText("Achtung!: Bitte geben Sie einen passende Nummer an");
							correctInput = false;
						}
						if(correctInput == true ) {
							lblNewLabel_forMessage.setText("");
							int roomsInt = Integer.parseInt(roomsinput); // beim parse werden wir dneke ich ein try catch brauchen
							int bedsInt = Integer.parseInt(bedinput);
							boolean temp = HotelMeth.createHotel(categoryinput, nameinput, user, contactinput, streetinput, numberinput, cityinput, citycodeinput, phoneinput, roomsInt, bedsInt);
							if( temp == true) { 
								JOptionPane.showMessageDialog(null, "Hotel wurde erfolgreich angelegt");
								dispose();					
							}else {
								JOptionPane.showMessageDialog(null, "Hotel wurde nicht angelegt");
							}
							
						}
					}
					
					
				}
			});
			
			button_Stop.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					dispose();
					
				}
			});
		}
}
