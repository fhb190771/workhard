package baseWindows;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Panel;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Button;
import javax.swing.JPasswordField;

public class UserPWChange extends JFrame {
	
	//TODO ordentlich benennen
	//TODO pr�fungen machen
	//TODO Exceptions werfen
	

	private JPanel contentPane;
	private JTextField textField_USERNAME;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserPWChange frame = new UserPWChange();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserPWChange() {
		setBackground(new Color(244,241,240));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 492, 361);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Panel panelMain = new Panel();
		panelMain.setBackground(new Color(203, 193, 185));
		panelMain.setBounds(10, 10, 458, 263);
		contentPane.add(panelMain);
		GridBagLayout gbl_panelMain = new GridBagLayout();
		gbl_panelMain.columnWidths = new int[] {5, 5, 5};
		gbl_panelMain.rowHeights = new int[]{15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelMain.columnWeights = new double[]{0.0, 1.0};
		gbl_panelMain.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelMain.setLayout(gbl_panelMain);
		
		JLabel lblNewLabel_Header = new JLabel("Passwort \u00E4ndern");
		lblNewLabel_Header.setForeground(new Color(69, 59, 52));
		lblNewLabel_Header.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_Header.setBackground(new Color(236, 232, 228));
		GridBagConstraints gbc_lblNewLabel_Header = new GridBagConstraints();
		gbc_lblNewLabel_Header.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_Header.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_Header.anchor = GridBagConstraints.NORTH;
		gbc_lblNewLabel_Header.gridx = 1;
		gbc_lblNewLabel_Header.gridy = 1;
		panelMain.add(lblNewLabel_Header, gbc_lblNewLabel_Header);
		
		JLabel beschrUSER = new JLabel("Username");
		GridBagConstraints gbc_beschrUSER = new GridBagConstraints();
		gbc_beschrUSER.anchor = GridBagConstraints.EAST;
		gbc_beschrUSER.insets = new Insets(0, 0, 5, 5);
		gbc_beschrUSER.gridx = 0;
		gbc_beschrUSER.gridy = 4;
		panelMain.add(beschrUSER, gbc_beschrUSER);
		
		textField_USERNAME = new JTextField();
		GridBagConstraints gbc_textField_USERNAME = new GridBagConstraints();
		gbc_textField_USERNAME.insets = new Insets(0, 0, 5, 5);
		gbc_textField_USERNAME.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_USERNAME.gridx = 1;
		gbc_textField_USERNAME.gridy = 4;
		panelMain.add(textField_USERNAME, gbc_textField_USERNAME);
		textField_USERNAME.setColumns(10);
		
		JLabel beschrPASSWORD = new JLabel("Password");
		GridBagConstraints gbc_beschrPASSWORD = new GridBagConstraints();
		gbc_beschrPASSWORD.anchor = GridBagConstraints.EAST;
		gbc_beschrPASSWORD.insets = new Insets(0, 0, 5, 5);
		gbc_beschrPASSWORD.gridx = 0;
		gbc_beschrPASSWORD.gridy = 5;
		panelMain.add(beschrPASSWORD, gbc_beschrPASSWORD);
		
		passwordField = new JPasswordField();
		GridBagConstraints gbc_passwordField = new GridBagConstraints();
		gbc_passwordField.insets = new Insets(0, 0, 5, 5);
		gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField.gridx = 1;
		gbc_passwordField.gridy = 5;
		panelMain.add(passwordField, gbc_passwordField);
		
		JLabel lblPasswordWiederholen = new JLabel("Password  wdh.");
		GridBagConstraints gbc_lblPasswordWiederholen = new GridBagConstraints();
		gbc_lblPasswordWiederholen.anchor = GridBagConstraints.EAST;
		gbc_lblPasswordWiederholen.insets = new Insets(0, 0, 5, 5);
		gbc_lblPasswordWiederholen.gridx = 0;
		gbc_lblPasswordWiederholen.gridy = 6;
		panelMain.add(lblPasswordWiederholen, gbc_lblPasswordWiederholen);
		
		passwordField_1 = new JPasswordField();
		GridBagConstraints gbc_passwordField_1 = new GridBagConstraints();
		gbc_passwordField_1.insets = new Insets(0, 0, 5, 5);
		gbc_passwordField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordField_1.gridx = 1;
		gbc_passwordField_1.gridy = 6;
		panelMain.add(passwordField_1, gbc_passwordField_1);
		
		JButton SpeichernButton = new JButton("Speichern");
		SpeichernButton.setBounds(379, 290, 89, 23);
		contentPane.add(SpeichernButton);
		
		JButton abbrechenButton = new JButton("Abbrechen");
		abbrechenButton.setBounds(280, 290, 89, 23);
		contentPane.add(abbrechenButton);
	}
}
