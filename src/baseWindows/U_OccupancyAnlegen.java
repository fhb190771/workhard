package baseWindows;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import occupancy.OccupancyMeth;
import run.Main_Main_main;
import werkzeugkasten.Utittity;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Panel;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Button;
import javax.swing.LayoutStyle.ComponentPlacement;

public class U_OccupancyAnlegen extends JFrame {
	
	//TODO ordentlich benennen
	//TODO pr�fungen machen
	//TODO Exceptions werfen
	

	private JPanel contentPane;
	private JTextField textField_MONAT;
	private JTextField textField_JAHR;
	private JTextField textField_BETTEN;
	private JTextField textField_REAUME;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					U_OccupancyAnlegen frame = new U_OccupancyAnlegen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public U_OccupancyAnlegen() {
		setResizable(false);
		setBackground(new Color(244,241,240));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 400);		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(236, 232, 228));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel_HEAD = new JLabel("Anlage einer Belegung");
		lblNewLabel_HEAD.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel_HEAD.setForeground(new Color(98, 0, 55));
		lblNewLabel_HEAD.setFont(new Font("Calibri", Font.BOLD, 20));

		
		JPanel panelMain = new JPanel();
		panelMain.setBorder(new LineBorder(new Color(126,110,98)));
		panelMain.setBackground(new Color(236,232,228));
		
		JButton button_Stop = new JButton("Abbrechen");
		button_Stop.setForeground(Color.BLACK);
		button_Stop.setFont(new Font("Calibri", Font.PLAIN, 14));
		button_Stop.setBackground(new Color(120, 104, 91));
		
		JButton button_Save = new JButton("Speichern");
		button_Save.setForeground(Color.BLACK);
		button_Save.setFont(new Font("Calibri", Font.PLAIN, 14));
		button_Save.setBackground(new Color(120, 104, 91));
		
		JLabel labelMessage = new JLabel("");
		labelMessage.setEnabled(false);
		labelMessage.setVerticalAlignment(SwingConstants.TOP);
		labelMessage.setHorizontalAlignment(SwingConstants.LEFT);
		labelMessage.setForeground(new Color(98, 0, 55));
		labelMessage.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		

		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(50)
							.addComponent(lblNewLabel_HEAD, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(21)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(labelMessage, GroupLayout.PREFERRED_SIZE, 397, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(button_Stop, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(button_Save, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE))
								.addComponent(panelMain, GroupLayout.PREFERRED_SIZE, 696, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(69, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_HEAD, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
					.addComponent(panelMain, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(button_Save, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
								.addComponent(button_Stop, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(labelMessage, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addGap(32))
		);
		contentPane.setLayout(gl_contentPane);
		
		
		JLabel beschrMonat = new JLabel("Monat");
		beschrMonat.setVerticalAlignment(SwingConstants.BOTTOM);
		beschrMonat.setHorizontalAlignment(SwingConstants.RIGHT);
		beschrMonat.setForeground(new Color(98,0,55));
		beschrMonat.setFont(new Font("Calibri", Font.PLAIN, 14));
		
		textField_MONAT = new JTextField();
		beschrMonat.setLabelFor(textField_MONAT);
		textField_MONAT.setFont(new Font("Calibri", Font.PLAIN, 14));
		textField_MONAT.setColumns(10);
		
		JLabel beschrJahr = new JLabel("Jahr");
		beschrJahr.setVerticalAlignment(SwingConstants.BOTTOM);
		beschrJahr.setHorizontalAlignment(SwingConstants.RIGHT);
		beschrJahr.setForeground(new Color(98, 0, 55));
		beschrJahr.setFont(new Font("Calibri", Font.PLAIN, 14));
		
		textField_JAHR = new JTextField();
		textField_JAHR.setForeground(Color.BLACK);
		textField_JAHR.setFont(new Font("Calibri", Font.PLAIN, 14));
		textField_JAHR.setColumns(10);
		
		JLabel beschrBelBetten = new JLabel("belegte Betten");
		beschrBelBetten.setVerticalAlignment(SwingConstants.BOTTOM);
		beschrBelBetten.setHorizontalAlignment(SwingConstants.RIGHT);
		beschrBelBetten.setForeground(new Color(98, 0, 55));
		beschrBelBetten.setFont(new Font("Calibri", Font.PLAIN, 14));
		
		textField_BETTEN = new JTextField();
		textField_BETTEN.setFont(new Font("Calibri", Font.PLAIN, 14));
		textField_BETTEN.setColumns(10);
		
		JLabel beschrBelRaeume = new JLabel("belegte Zimmer");
		beschrBelRaeume.setVerticalAlignment(SwingConstants.BOTTOM);
		beschrBelRaeume.setHorizontalAlignment(SwingConstants.RIGHT);
		beschrBelRaeume.setForeground(new Color(98, 0, 55));
		beschrBelRaeume.setFont(new Font("Calibri", Font.PLAIN, 14));
		
		textField_REAUME = new JTextField();
		textField_REAUME.setFont(new Font("Calibri", Font.PLAIN, 14));
		textField_REAUME.setColumns(10);
		
		
		GroupLayout gl_panelMain = new GroupLayout(panelMain);
		gl_panelMain.setHorizontalGroup(
			gl_panelMain.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelMain.createSequentialGroup()
					.addGap(18)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.TRAILING)
						.addComponent(beschrMonat)
						.addGroup(gl_panelMain.createParallelGroup(Alignment.LEADING)
							.addComponent(beschrBelRaeume)
							.addComponent(beschrBelBetten))
						.addComponent(beschrJahr, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.TRAILING)
						.addComponent(textField_MONAT, GroupLayout.PREFERRED_SIZE, 309, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_BETTEN, GroupLayout.PREFERRED_SIZE, 309, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_REAUME, GroupLayout.PREFERRED_SIZE, 309, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_JAHR, GroupLayout.PREFERRED_SIZE, 309, GroupLayout.PREFERRED_SIZE))
					.addGap(221))
		);
		gl_panelMain.setVerticalGroup(
			gl_panelMain.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelMain.createSequentialGroup()
					.addGap(19)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.BASELINE)
						.addComponent(beschrMonat)
						.addComponent(textField_MONAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.BASELINE)
						.addComponent(beschrJahr)
						.addComponent(textField_JAHR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.BASELINE)
						.addComponent(beschrBelBetten)
						.addComponent(textField_BETTEN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.BASELINE)
						.addComponent(beschrBelRaeume)
						.addComponent(textField_REAUME, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(55, Short.MAX_VALUE))
		);
		panelMain.setLayout(gl_panelMain);
		
		button_Save.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean correctInput = true;
				String monthInp = textField_MONAT.getText();
				if(monthInp.isEmpty()) {
					JOptionPane.showMessageDialog(beschrMonat, "Es wurde kein Monat eingetragen");
				}
				String yearInp = textField_JAHR.getText();
				if(yearInp.isEmpty()) {
					JOptionPane.showMessageDialog(beschrJahr, "Es wurde kein Jahr eingetragen");
				}
				String bedInp = textField_BETTEN.getText();
				if(bedInp.isEmpty()) {
					JOptionPane.showMessageDialog(beschrBelBetten, "Es wurde keine Bettenanzahl eingetragen");
				}
				String roomInp = textField_REAUME.getText();
				if(roomInp.isEmpty()) {
					JOptionPane.showMessageDialog(beschrBelRaeume, "Es wurde keine Raumanzahl eingetragen");
				}
				if(Utittity.nurziffern(monthInp) == false) {
					labelMessage.setText("Bitte im Monatsfeld nur Ziffern angeben");
					correctInput = false;
				}
				if(Utittity.nurziffern(yearInp)==false) {
					labelMessage.setText("Bitte im Jahresfeld nur Ziffern angeben");
					correctInput = false;
				}
				if(Utittity.nurziffern(bedInp)==false) {
					labelMessage.setText("Bitte im Bettenfeld nur Ziffern angeben");
					correctInput = false;
				}
				if(Utittity.nurziffern(roomInp)==false) {
					labelMessage.setText("Bitte im Raumfeld nur Ziffern angeben");
					correctInput = false;
				}
				if(correctInput == true) {
					labelMessage.setText("");
					int monthInt = Integer.parseInt(monthInp);
					int yearInt = Integer.parseInt(yearInp);
					int bedInt = Integer.parseInt(bedInp);
					int roomInt = Integer.parseInt(roomInp);
					boolean temp = OccupancyMeth.createOccupancy(monthInt, yearInt, roomInt, bedInt, Main_Main_main.getLosername());
					if( temp == true) { //TODO davor check if exists boolean createOccupancy(String username, int month, int year, int beds, int rooms)
						JOptionPane.showMessageDialog(null, "Occupancy wurde erfolgreich angelegt");
						dispose();
					}else {
						JOptionPane.showMessageDialog(null, "Occupancy wurde nicht angelegt");
					}
				}
				
			}
		});
		
		button_Stop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				
			}
		});
	}
}
