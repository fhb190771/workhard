package baseWindows;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import werkzeugkasten.Utittity;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Panel;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Button;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JCheckBox;
import javax.swing.JPasswordField;

public class A_UserAendern extends JFrame {
	
	//TODO under construction
	

	private JPanel contentPane;
	private JTextField textField_Username;
	private JPasswordField pFielPasswort;
	private JPasswordField pFieldPasswort;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A_UserAendern frame = new A_UserAendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public A_UserAendern() {
		setResizable(false);
		setBackground(new Color(244,241,240));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 400);		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(236, 232, 228));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel_HEAD = new JLabel("�nderung eines Users");
		lblNewLabel_HEAD.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel_HEAD.setForeground(new Color(98, 0, 55));
		lblNewLabel_HEAD.setFont(new Font("Calibri", Font.BOLD, 20));

		
		JPanel panelMain = new JPanel();
		panelMain.setBorder(new LineBorder(new Color(126,110,98)));
		panelMain.setBackground(new Color(236,232,228));
		
		JButton button_Stop = new JButton("Abbrechen");
		button_Stop.setForeground(Color.BLACK);
		button_Stop.setFont(new Font("Calibri", Font.PLAIN, 14));
		button_Stop.setBackground(new Color(120, 104, 91));
		
		JButton button_Update = new JButton("�ndern");
		button_Update.setForeground(Color.BLACK);
		button_Update.setFont(new Font("Calibri", Font.PLAIN, 14));
		button_Update.setBackground(new Color(120, 104, 91));
		
		JLabel labelMessage = new JLabel("");
		labelMessage.setEnabled(false);
		labelMessage.setVerticalAlignment(SwingConstants.TOP);
		labelMessage.setHorizontalAlignment(SwingConstants.LEFT);
		labelMessage.setForeground(new Color(98, 0, 55));
		labelMessage.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		

		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(50)
							.addComponent(lblNewLabel_HEAD, GroupLayout.PREFERRED_SIZE, 192, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(21)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(labelMessage, GroupLayout.PREFERRED_SIZE, 397, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(button_Stop, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(button_Update, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE))
								.addComponent(panelMain, GroupLayout.PREFERRED_SIZE, 696, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(69, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_HEAD, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
					.addComponent(panelMain, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(button_Update, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
								.addComponent(button_Stop, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(labelMessage, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addGap(32))
		);
		contentPane.setLayout(gl_contentPane);
		
		
		JLabel beschrUsername = new JLabel("Username");
		beschrUsername.setVerticalAlignment(SwingConstants.BOTTOM);
		beschrUsername.setHorizontalAlignment(SwingConstants.RIGHT);
		beschrUsername.setForeground(new Color(98,0,55));
		beschrUsername.setFont(new Font("Calibri", Font.PLAIN, 14));
		
		textField_Username = new JTextField();
		beschrUsername.setLabelFor(textField_Username);
		textField_Username.setFont(new Font("Calibri", Font.PLAIN, 14));
		textField_Username.setColumns(10);
		
		JLabel beschrPasswort = new JLabel("Passwort");
		beschrPasswort.setVerticalAlignment(SwingConstants.BOTTOM);
		beschrPasswort.setHorizontalAlignment(SwingConstants.RIGHT);
		beschrPasswort.setForeground(new Color(98, 0, 55));
		beschrPasswort.setFont(new Font("Calibri", Font.PLAIN, 14));
		
		JLabel beschrPasswortWdh = new JLabel("Passwort wiederholen");
		beschrPasswortWdh.setVerticalAlignment(SwingConstants.BOTTOM);
		beschrPasswortWdh.setHorizontalAlignment(SwingConstants.RIGHT);
		beschrPasswortWdh.setForeground(new Color(98, 0, 55));
		beschrPasswortWdh.setFont(new Font("Calibri", Font.PLAIN, 14));
		
		JLabel beschrisAdmin = new JLabel("ist Admin ?");
		beschrisAdmin.setVerticalAlignment(SwingConstants.BOTTOM);
		beschrisAdmin.setHorizontalAlignment(SwingConstants.RIGHT);
		beschrisAdmin.setForeground(new Color(98, 0, 55));
		beschrisAdmin.setFont(new Font("Calibri", Font.PLAIN, 14));
		
		JCheckBox checkboxIsAdmin = new JCheckBox("");
		
		pFielPasswort = new JPasswordField();
		
		pFieldPasswort = new JPasswordField();
		
		
		GroupLayout gl_panelMain = new GroupLayout(panelMain);
		gl_panelMain.setHorizontalGroup(
			gl_panelMain.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelMain.createSequentialGroup()
					.addGap(18)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panelMain.createParallelGroup(Alignment.TRAILING, false)
							.addComponent(beschrUsername, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(beschrPasswortWdh, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(beschrPasswort, 0, 0, Short.MAX_VALUE))
						.addComponent(beschrisAdmin))
					.addPreferredGap(ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelMain.createParallelGroup(Alignment.LEADING, false)
							.addComponent(textField_Username, GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
							.addComponent(checkboxIsAdmin)
							.addComponent(pFielPasswort))
						.addComponent(pFieldPasswort, GroupLayout.PREFERRED_SIZE, 309, GroupLayout.PREFERRED_SIZE))
					.addGap(221))
		);
		gl_panelMain.setVerticalGroup(
			gl_panelMain.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelMain.createSequentialGroup()
					.addGap(19)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.BASELINE)
						.addComponent(beschrUsername)
						.addComponent(textField_Username, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.BASELINE)
						.addComponent(beschrPasswort)
						.addComponent(pFielPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.BASELINE)
						.addComponent(beschrPasswortWdh)
						.addComponent(pFieldPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(15)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.TRAILING)
						.addComponent(beschrisAdmin)
						.addComponent(checkboxIsAdmin))
					.addContainerGap(61, Short.MAX_VALUE))
		);
		panelMain.setLayout(gl_panelMain);
		
		button_Update.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean correctInput = true;	
				String newusername = textField_Username.getText();
				String pw1 = String.valueOf(pFielPasswort.getPassword());
				String pw2 = String.valueOf(pFieldPasswort.getPassword());
						if(newusername.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Es wurde kein Username eingegeben");
						}
						if(pw1.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Die Eingabe des ersten Passwortes fehlt");
						}
						if(pw2.isEmpty()) {
							JOptionPane.showMessageDialog(null, "Die Eingabe des zweiten Passwortes fehlt");
						}
						if(pw1.length() < 8) { 
							labelMessage.setText("das Passwort musst mindestens 8 Zeichen lang sein");
							correctInput = false;
						}	
						if(!pw1.equals(pw2)) { 
							labelMessage.setText("Die von Ihnen eingegebenen Passw�rter stimmen nicht �berein");
							correctInput = false;
						}	
						if(correctInput == true ) {
							labelMessage.setText("");
							boolean isAdmin = checkboxIsAdmin.isSelected();
							boolean temp = true;
							if( temp == true) { //TODO boolean changeUser(String username, String newusername, String password, boolean isAdmin)
								JOptionPane.showMessageDialog(null, "User wurde erfolgreich ge�ndert");
								dispose();
							}else {
								JOptionPane.showMessageDialog(null, "User wurde nicht ge�ndert");
							}
							
						}
			}
		});
		
		button_Stop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				
			}
		});
	}
}
