package baseWindows;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.GridBagLayout;
import javax.swing.JRadioButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import java.awt.CardLayout;
import java.awt.Color;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import hotel.Hotel;
import hotel.HotelMeth;
import owner.OwnerMeth;
import run.Main_Main_main;

import javax.swing.JList;
import javax.swing.BoxLayout;
import javax.swing.JInternalFrame;
import javax.swing.JSplitPane;
import javax.swing.AbstractListModel;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import javax.swing.JTextField;

public class MainWindowHotelUser extends JFrame {

	private JPanel contentPane;
	private JTable tableUserMainpage;
	private JTextField textField_Hotelaname;
	private JTextField textField_Adresse_1;
	private JTextField textField_Adresse_2;
	private JTextField textField_nummer;
	private JTextField textField_Mail;
	//TODO: MainWindow anpassen

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindowHotelUser frame = new MainWindowHotelUser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindowHotelUser() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 719, 465);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu menueBenutzer = new JMenu("Benutzer");
		menuBar.add(menueBenutzer);
		
		JMenuItem bp_anpasse = new JMenuItem("Benutzerprofil anpassen");
		menueBenutzer.add(bp_anpasse);
		
		JMenu menueOcup = new JMenu("Occupancy");
		menuBar.add(menueOcup);
		
		JMenuItem occu_anlegen = new JMenuItem("Occupancy anlegen");
		menueOcup.add(occu_anlegen);
		
		JMenuItem occu_aendern = new JMenuItem("Occupancy \u00C4ndern");
		menueOcup.add(occu_aendern);
		
		JMenuItem massimp_occ = new JMenuItem("Massenimport (CSV)");
		menueOcup.add(massimp_occ);
		
		JMenuItem statistic_print_occ = new JMenuItem("eigene statistik ausgeben");
		menueOcup.add(statistic_print_occ);
		
		JMenu menueHotel = new JMenu("Hotel");
		menuBar.add(menueHotel);
		
		JMenuItem hot_anlegen = new JMenuItem("Neues Hotel");
		menueHotel.add(hot_anlegen);
		
		JMenuItem hot_bearbeiten = new JMenuItem("Hotel bearbeiten");
		menueHotel.add(hot_bearbeiten);
		
		JMenuItem hot_loeschen = new JMenuItem("Hotel l\u00F6schen");
		menueHotel.add(hot_loeschen);
		
		JMenu menueHelp = new JMenu("Help");
		menuBar.add(menueHelp);
		
		JMenuItem saveandexitButton = new JMenuItem("Save & Exit");
		menuBar.add(saveandexitButton);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		JTabbedPane mainPanel = new JTabbedPane(JTabbedPane.TOP);
		mainPanel.setFont(new Font("Calibri", Font.BOLD, 10));
		mainPanel.setForeground(new Color(164,26,62));
		mainPanel.setBackground(new Color(94,80,70));
		contentPane.add(mainPanel);
		
		JSplitPane mainPanelUserMainwindow = new JSplitPane();
		mainPanel.addTab("Mein Hotel", null, mainPanelUserMainwindow, null);
		
		tableUserMainpage = new JTable();
		tableUserMainpage.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
				{null, null, null, null, null},
			},
			new String[] {
				"Monat", "Zimmer", "Zimmerbelegung %", "Betten", "Bettenbelegung (%)"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, true, false, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tableUserMainpage.getColumnModel().getColumn(2).setPreferredWidth(109);
		tableUserMainpage.getColumnModel().getColumn(4).setPreferredWidth(117);
		mainPanelUserMainwindow.setRightComponent(tableUserMainpage);
		
		Hotel hoteldaten;
		
		 
		if(Main_Main_main.getLoserOwner().getHotel() != null) {
			 hoteldaten  = HotelMeth.getHotelByOwner(Main_Main_main.getLosername());
		}else{
			 hoteldaten = new Hotel("","", "", "", "", "", "", "","",00, 00);
		}
		
		
		
		
		JPanel panel = new JPanel();
		mainPanelUserMainwindow.setLeftComponent(panel);
		panel.setBackground(new Color(236, 232, 228));
		
		JLabel lblNewLabel_Hotelname = new JLabel(" Hotelname:");
		lblNewLabel_Hotelname.setFont(new Font("Calibri", Font.BOLD, 14));
		lblNewLabel_Hotelname.setForeground(new Color(98, 0, 55));
		
		JLabel lblNewLabel_Adresse = new JLabel("Adresse:");
		lblNewLabel_Adresse.setFont(new Font("Calibri", Font.BOLD, 14));
		lblNewLabel_Adresse.setForeground(new Color(98, 0, 55));
		
		textField_Hotelaname = new JTextField();
		textField_Hotelaname.setEditable(false);
		textField_Hotelaname.setFont(new Font("Calibri", Font.PLAIN, 10));
		textField_Hotelaname.setForeground(Color.BLACK);
		textField_Hotelaname.setColumns(10);
		textField_Hotelaname.setText(hoteldaten.getHotel_name());
		
		textField_Adresse_1 = new JTextField();
		textField_Adresse_1.setText((String) null);
		textField_Adresse_1.setForeground(Color.BLACK);
		textField_Adresse_1.setFont(new Font("Calibri", Font.PLAIN, 10));
		textField_Adresse_1.setEditable(false);
		textField_Adresse_1.setColumns(10);
		textField_Adresse_1.setText(hoteldaten.getHotel_address() + ", "+ hoteldaten.getHotel_addressNumber());
		
		textField_Adresse_2 = new JTextField();
		textField_Adresse_2.setText((String) null);
		textField_Adresse_2.setForeground(Color.BLACK);
		textField_Adresse_2.setFont(new Font("Calibri", Font.PLAIN, 10));
		textField_Adresse_2.setEditable(false);
		textField_Adresse_2.setColumns(10);
		textField_Adresse_2.setText(hoteldaten.getHotel_cityCode() +", "+ hoteldaten.getHotel_city());
		
		JLabel lblNewLabel_Nummer = new JLabel("Telefonnummer:");
		lblNewLabel_Nummer.setForeground(new Color(98, 0, 55));
		lblNewLabel_Nummer.setFont(new Font("Calibri", Font.BOLD, 14));
		
		textField_nummer = new JTextField();
		textField_nummer.setText((String) null);
		textField_nummer.setForeground(Color.BLACK);
		textField_nummer.setFont(new Font("Calibri", Font.PLAIN, 10));
		textField_nummer.setEditable(false);
		textField_nummer.setColumns(10);
		textField_nummer.setText(hoteldaten.getHotel_phone());
		
		JLabel lblNewLabel_Mail = new JLabel("E-Mail:");
		lblNewLabel_Mail.setForeground(new Color(98, 0, 55));
		lblNewLabel_Mail.setFont(new Font("Calibri", Font.BOLD, 14));
		
		textField_Mail = new JTextField();
		textField_Mail.setText((String) null);
		textField_Mail.setForeground(Color.BLACK);
		textField_Mail.setFont(new Font("Calibri", Font.PLAIN, 10));
		textField_Mail.setEditable(false);
		textField_Mail.setColumns(10);
		textField_Mail.setText(hoteldaten.getHotel_contact());
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(textField_Adresse_1, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(5)
							.addComponent(lblNewLabel_Hotelname))
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel_Adresse))
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(textField_Adresse_2, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(textField_Hotelaname, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_Nummer, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(76, Short.MAX_VALUE))
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(textField_nummer, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_Mail, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(76, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(textField_Mail, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(5)
					.addComponent(lblNewLabel_Hotelname)
					.addGap(3)
					.addComponent(textField_Hotelaname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblNewLabel_Adresse)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField_Adresse_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField_Adresse_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblNewLabel_Nummer, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField_nummer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblNewLabel_Mail, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField_Mail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(134, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		
	//--------------------------Action Listener Anfang--------------------------------------------------
	bp_anpasse.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			//
			
		}
	});
	
	occu_anlegen.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
		
			new U_OccupancyAnlegen().setVisible(true);
			
		}
	});

	
	occu_aendern.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			new U_OccupancyAendern().setVisible(true);
			
		}
	} );
	
	massimp_occ.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			new U_OccupancyAnlegenCSVforLater().setVisible(true);
			
		}
	});
	
	statistic_print_occ.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			// das wiss ma no ned 
		}
	});
	
	
	hot_anlegen.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
		
			new U_HotelAnlegen().setVisible(true);
		}
	});
	
	hot_bearbeiten.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			new U_HotelAendern().setVisible(true);
			
		}
	});
	
	hot_loeschen.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
		
			new U_HotelLoeschen().setVisible(true);
			
		}
	});
	
	saveandexitButton.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			

			//wiss ma auch noch ned ? 
			
			
		}
	});
	
	
	
	
	//--------------------------Action Listener Ende --------------------------------------------------
	
	
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
		
		
	}
}
