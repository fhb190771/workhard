package baseWindows;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import hotel.HotelMeth;
import owner.OwnerMeth;
import run.Main_Main_main;
import werkzeugkasten.Utittity;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Panel;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Button;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JCheckBox;
import javax.swing.JPasswordField;

public class A_HotelLoeschen extends JFrame {
	
	//TODO ordentlich benennen
	//TODO pr�fungen machen
	//TODO Exceptions werfen
	

	private JPanel contentPane;
	private JTextField textField_Benutzer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A_HotelLoeschen frame = new A_HotelLoeschen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public A_HotelLoeschen() {
		setResizable(false);
		setBackground(new Color(244,241,240));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 400);		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(236, 232, 228));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		
		JPanel panelMain = new JPanel();
		panelMain.setBorder(new LineBorder(new Color(126,110,98)));
		panelMain.setBackground(new Color(236,232,228));
		
		JButton button_Stop = new JButton("Abbrechen");
		button_Stop.setForeground(Color.BLACK);
		button_Stop.setFont(new Font("Calibri", Font.PLAIN, 14));
		button_Stop.setBackground(new Color(120, 104, 91));
		
		JButton btnLschen = new JButton("L\u00F6schen");
		btnLschen.setForeground(Color.BLACK);
		btnLschen.setFont(new Font("Calibri", Font.PLAIN, 14));
		btnLschen.setBackground(new Color(120, 104, 91));
		
		JLabel labelMessage = new JLabel("");
		labelMessage.setEnabled(false);
		labelMessage.setVerticalAlignment(SwingConstants.TOP);
		labelMessage.setHorizontalAlignment(SwingConstants.LEFT);
		labelMessage.setForeground(new Color(98, 0, 55));
		labelMessage.setFont(new Font("Calibri", Font.BOLD | Font.ITALIC, 14));
		

		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(labelMessage, GroupLayout.PREFERRED_SIZE, 397, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(button_Stop, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnLschen, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE))
						.addComponent(panelMain, GroupLayout.PREFERRED_SIZE, 696, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(69, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(59, Short.MAX_VALUE)
					.addComponent(panelMain, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnLschen, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
								.addComponent(button_Stop, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(labelMessage, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
					.addGap(32))
		);
		contentPane.setLayout(gl_contentPane);
		
		JLabel lblNewLabel_HEAD = new JLabel("F\u00FCr welchen Benutzer soll das Hotel gel\u00F6scht werden?");
		lblNewLabel_HEAD.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel_HEAD.setForeground(new Color(98, 0, 55));
		lblNewLabel_HEAD.setFont(new Font("Calibri", Font.BOLD, 20));
		
		JLabel lblNewLabel_HEAD_1 = new JLabel("Hinweis: Alle dazugeh\u00F6rigen Belegungdaten werden auch gel\u00F6scht");
		lblNewLabel_HEAD_1.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel_HEAD_1.setForeground(new Color(98, 0, 55));
		lblNewLabel_HEAD_1.setFont(new Font("Calibri", Font.PLAIN, 20));
		
		textField_Benutzer = new JTextField();
		textField_Benutzer.setFont(new Font("Calibri", Font.PLAIN, 14));
		textField_Benutzer.setColumns(10);
		
		
		GroupLayout gl_panelMain = new GroupLayout(panelMain);
		gl_panelMain.setHorizontalGroup(
			gl_panelMain.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelMain.createSequentialGroup()
					.addGap(27)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_HEAD, GroupLayout.PREFERRED_SIZE, 641, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_HEAD_1, GroupLayout.PREFERRED_SIZE, 641, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_Benutzer, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(26, Short.MAX_VALUE))
		);
		gl_panelMain.setVerticalGroup(
			gl_panelMain.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelMain.createSequentialGroup()
					.addGap(42)
					.addComponent(lblNewLabel_HEAD, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField_Benutzer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNewLabel_HEAD_1, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(79, Short.MAX_VALUE))
		);
		panelMain.setLayout(gl_panelMain);
		
		btnLschen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String user = textField_Benutzer.getText();
				if(OwnerMeth.checkIfOwnerExists(user) == false) {
					JOptionPane.showMessageDialog(lblNewLabel_HEAD, "Dieser Benutzer existiert nicht!");
				}else {
					if(HotelMeth.deleteHotel(user) == true) { 
						JOptionPane.showMessageDialog(null, "Hotel wurde erfolgreich gel�scht");
						dispose();
					}else {
						JOptionPane.showMessageDialog(null, "Hotel wurde nicht gel�scht");
					}
				}
				
			}
		});
		
		button_Stop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				
			}
		});
	}
}

