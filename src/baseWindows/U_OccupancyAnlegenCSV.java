package baseWindows;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import werkzeugkasten.Utittity;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;

import javax.management.loading.PrivateClassLoader;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Panel;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.Button;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JCheckBox;
import javax.swing.JPasswordField;

public class U_OccupancyAnlegenCSV extends JFrame {
	
	//TODO ordentlich benennen
	//TODO pr�fungen machen
	//TODO Exceptions werfen
	

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				File f = null;
				try {
					U_OccupancyAnlegenCSV frame = new U_OccupancyAnlegenCSV();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public U_OccupancyAnlegenCSV() {
		setResizable(false);
		setBackground(new Color(244,241,240));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 400);		
		contentPane = new JPanel();
		contentPane.setBackground(new Color(236, 232, 228));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		
		JPanel panelMain = new JPanel();
		panelMain.setBorder(new LineBorder(new Color(126,110,98)));
		panelMain.setBackground(new Color(236,232,228));
		
		JButton button_Stop = new JButton("Abbrechen");
		button_Stop.setForeground(Color.BLACK);
		button_Stop.setFont(new Font("Calibri", Font.PLAIN, 14));
		button_Stop.setBackground(new Color(120, 104, 91));
		
		JButton button_FileSuchen = new JButton("File suchen");
		button_FileSuchen.setForeground(Color.BLACK);
		button_FileSuchen.setFont(new Font("Calibri", Font.PLAIN, 14));
		button_FileSuchen.setBackground(new Color(120, 104, 91));
		
		
		
		

		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(button_Stop, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(button_FileSuchen, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE))
						.addComponent(panelMain, GroupLayout.PREFERRED_SIZE, 696, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(30, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(59, Short.MAX_VALUE)
					.addComponent(panelMain, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(button_FileSuchen)
						.addComponent(button_Stop, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
					.addGap(32))
		);
		contentPane.setLayout(gl_contentPane);
		
		JLabel lblNewLabel_HEAD = new JLabel("Massenimport um Belegungsdaten f\u00FCr mehrere Monate nachzutragen.");
		lblNewLabel_HEAD.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel_HEAD.setForeground(new Color(98, 0, 55));
		lblNewLabel_HEAD.setFont(new Font("Calibri", Font.BOLD, 20));
		
		JTextArea txtrBeschreibung = new JTextArea();
		txtrBeschreibung.setBackground(new Color (203,193,185));
		txtrBeschreibung.setEditable(false);
		txtrBeschreibung.setText("Beschreibung wie csv aufgebaut sein muss");
		txtrBeschreibung.setForeground(Color.black);
		txtrBeschreibung.setFont(new Font("Calibri", Font.PLAIN, 14));
		
		
		GroupLayout gl_panelMain = new GroupLayout(panelMain);
		gl_panelMain.setHorizontalGroup(
			gl_panelMain.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelMain.createSequentialGroup()
					.addGap(19)
					.addGroup(gl_panelMain.createParallelGroup(Alignment.LEADING)
						.addComponent(txtrBeschreibung, GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE)
						.addComponent(lblNewLabel_HEAD, GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE))
					.addGap(18))
		);
		gl_panelMain.setVerticalGroup(
			gl_panelMain.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelMain.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_HEAD, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(txtrBeschreibung, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(78, Short.MAX_VALUE))
		);
		panelMain.setLayout(gl_panelMain);
		
		button_FileSuchen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				JFileChooser jfc = new JFileChooser();

				if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					File f = jfc.getSelectedFile();
					//TODO: ka was man da jz noch machen muss :)
				}
				
				
				
			}
		});
		
		
		button_Stop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				
			}
		});
	}
	
	
}

