package baseWindows;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.GridBagLayout;
import javax.swing.JRadioButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import java.awt.CardLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MainWindowAdminUser extends JFrame {

	private JPanel contentPane;
	private JTable HotelTable;
	private JTable Belegungstable;
	// TODO: MainWindow anpassen

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindowAdminUser frame = new MainWindowAdminUser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindowAdminUser() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu menueBenutzer = new JMenu("Benutzer");
		menuBar.add(menueBenutzer);

		JMenuItem ben_anpassen = new JMenuItem("Benutzerprofil anpassen");
		menueBenutzer.add(ben_anpassen);

		JMenuItem ben_anlegen = new JMenuItem("Neuen Benutzer anlegen");
		menueBenutzer.add(ben_anlegen);

		JMenuItem ben_loe = new JMenuItem("Benutzer l\u00F6schen");
		menueBenutzer.add(ben_loe);

		JMenuItem ben_massImp = new JMenuItem("Massenanlage (CSV import)");
		menueBenutzer.add(ben_massImp);

		JMenu menueOcup = new JMenu("Occupancy");
		menuBar.add(menueOcup);

		JMenuItem occ_anl = new JMenuItem("Occupancy anlegen");
		menueOcup.add(occ_anl);

		JMenuItem occ_aen = new JMenuItem("Occupancy \u00C4ndern");
		menueOcup.add(occ_aen);
		
		JMenuItem occ_loe = new JMenuItem("Occupancy l�schen");
		menueOcup.add(occ_loe);

		JMenuItem occ_massImp = new JMenuItem("Massenimport (CSV)");
		menueOcup.add(occ_massImp);

		JMenuItem occ_einl_stat = new JMenuItem("einzelne statistik ausgeben");
		menueOcup.add(occ_einl_stat);

		JMenuItem pcc_gesamt_stat = new JMenuItem("gesamt Statistik ausgeben");
		menueOcup.add(pcc_gesamt_stat);

		JMenu menueHotel = new JMenu("Hotel");
		menuBar.add(menueHotel);

		JMenuItem hot_new = new JMenuItem("Neues Hotel");
		menueHotel.add(hot_new);

		JMenuItem hot_massImp = new JMenuItem("Massenimport (CSV)");
		menueHotel.add(hot_massImp);

		JMenuItem hot_change = new JMenuItem("Hotel bearbeiten");
		menueHotel.add(hot_change);

		JMenuItem hot_delete = new JMenuItem("Hotel l\u00F6schen");
		menueHotel.add(hot_delete);

		JMenu menueHelp = new JMenu("Help");
		menuBar.add(menueHelp);

		JMenuItem saveandexitButton = new JMenuItem("Save & Exit");
		menuBar.add(saveandexitButton);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));

		JTabbedPane mainPanel = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(mainPanel, "name_90696707497600");

		JTabbedPane HotelPanel = new JTabbedPane(JTabbedPane.TOP);
		mainPanel.addTab("Hotel", null, HotelPanel, null);

		HotelTable = new JTable();
		HotelTable.setCellSelectionEnabled(true);
		HotelTable.setColumnSelectionAllowed(true);
		HotelTable.setModel(new DefaultTableModel(new Object[][] { { "", null, null, null, null },
				{ null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null },
				{ null, null, null, null, null }, },
				new String[] { "Kategorie", "Zimmer", "Zimmer Auslastung (%)", "Betten ", "Betten Auslastung" }));
		HotelPanel.addTab("Hotel \u00DCbersicht", null, HotelTable, null);

		JTabbedPane OccupationPanel = new JTabbedPane(JTabbedPane.TOP);
		mainPanel.addTab("Belegung", null, OccupationPanel, null);

		Belegungstable = new JTable();
		OccupationPanel.addTab("Belegung f\u00FCr:", null, Belegungstable, null);

		// --------------------------Action Listener Anfang--------------------------------------------------

		ben_anlegen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				new A_UserAnlegen().setVisible(true);

			}
		});

		ben_anpassen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				//fehlt noch

			}
		});

		ben_loe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				new A_UserLoeschen().setVisible(true);
				
			}
		});

		ben_massImp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				// fehlt noch
				
			}
		});

		occ_aen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				new A_OccupancyAendern().setVisible(true);

			}
		});

		occ_loe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				new A_OccupancyLoeschen().setVisible(true);

			}
		});
		
		occ_anl.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				new A_OccupancyAnlegen().setVisible(true);

			}
		});

		occ_massImp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		occ_einl_stat.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// to be defined

			}
		});

		pcc_gesamt_stat.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		hot_change.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				new A_HotelAendern().setVisible(true);

			}
		});

		hot_delete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				new A_HotelLoeschen().setVisible(true);

			}
		});

		hot_massImp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		hot_new.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				new A_HotelAnlegen().setVisible(true);
				
			}
		});
		
		
		
		// --------------------------Action Listener Ende--------------------------------------------------

	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
