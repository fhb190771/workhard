package utility;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PrintIntro extends JFrame implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JPanel panel = new JPanel();
	JButton bt_print = new JButton("Print");
	JTextField tt = new JTextField(10);

	public static void main(String[] args) {

		PrintIntro gg = new PrintIntro();
		gg.setVisible(true);

	}

	public PrintIntro() {
		setSize(500, 500);
		add(panel);

		bt_print.addActionListener(this);
		panel.add(bt_print);
		panel.add(tt);

	}
	
	// ??? was diese?
	

	public static void printComponenet(Component component) {
		PrinterJob pj = PrinterJob.getPrinterJob();
		pj.setJobName(" Print Component ");

		pj.setPrintable(new Printable() {
			public int print(Graphics pg, PageFormat pf, int pageNum) {
				if (pageNum > 0) {
					return Printable.NO_SUCH_PAGE;
				}

				Graphics2D g2 = (Graphics2D) pg;
				g2.translate(pf.getImageableX(), pf.getImageableY());
				component.paint(g2);
				return Printable.PAGE_EXISTS;
			}
		});
		if (pj.printDialog() == false)
			return;

		try {
			pj.print();
		} catch (PrinterException ex) {
			// handle exception
		}
	}

	// ??? was diese?
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//
		if (e.getSource().equals(bt_print)) {
			printComponenet(panel);

		}

	}

}
