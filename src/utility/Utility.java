package utility;

public final class Utility {	//mann kann nicht erben, da final

  private Utility() {
  }

  public static int randomWithRange(int min, int max)
  {
     int range = (max - min) + 1;     
     return (int)(Math.random() * range) + min;
  }
  
  public static boolean isEmptyString(final String value) { 		//sind static alle
    return (value == null || "".equals(value.trim()));
  }

  public static boolean isLessOrEqualZero(final int value) {
    return (value <= 0);
  }

  public static boolean isLessOrEqualZero(final double value) {
    return (value <= 0);
  }

  public static boolean isLessZero(final int value) {
    return (value < 0);
  }

  public static boolean isLessZero(final double value) {
    return (value < 0);
  }

  public static boolean isGreaterOrEqualZero(final int value) {
    return (value >= 0);
  }

  public static boolean isGreaterOrEqualZero(final double value) {
    return (value >= 0);
  }

  public static boolean isGreaterZero(final int value) {
    return (value > 0);
  }

  public static boolean isGreaterZero(final double value) {
    return (value > 0);
  }


  public static boolean isInRange(final int value, final int minimum, final int maximum) {
    return value >= minimum && value <= maximum;
  }

  public static boolean isInRange(final double value, final double minimum, final double maximum) {
    return value >= minimum && value <= maximum;
  }

  public static boolean isGreater(final int value, final int limit) {
    return (value > limit);
  }

  public static boolean isGreater(final double value, final int limit) {
    return (value > limit);
  }

  public static boolean isGreaterEqual(final int value, final int limit) {
    return (value >= limit);
  }

  public static boolean isGreaterEqual(final double value, final int limit) {
    return (value >= limit);
  }


  public static boolean isLess(final int value, final int limit) {
    return (value < limit);
  }

  public static boolean isLess(final double value, final int limit) {
    return (value < limit);
  }

  public static boolean isLessEqual(final int value, final int limit) {
    return (value <= limit);
  }

  public static boolean isLessEqual(final double value, final int limit) {
    return (value <= limit);
  }

  public static String stripBrackets(String text) {
    if (isEmptyString(text) || text.length() < 2) {
      return text;
    }

    if(text.endsWith("]")) {
      text = text.substring(0,text.length() - 1);
    }
    if(text.startsWith("[")){
      text = text.substring(1);
    }

    return text;

  }


}
