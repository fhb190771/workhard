package werkzeugkasten;

public final class Utittity {

	// viel spa� mit den Utilities

	public static boolean stringPruefer(String uebergabeString, int maxlaenge, int minLaenge, boolean sonderzeichen, 
			boolean ziffern, boolean gro�KLEIN) {

		boolean ausgabe = false;
		if (uebergabeString != null) {
			if (ziffern) {
				if (enthealtZiffer(uebergabeString)) {
					ausgabe = true;
				} else {
					ausgabe = false;
					return ausgabe;
				}
			}
			if (sonderzeichen) {
				if (enthealtSonderzeiche(uebergabeString)) {
					ausgabe = true;
				} else {
					ausgabe = false;
					return ausgabe;
				}
			}
			if (gro�KLEIN) {
				if (enthealtGrossklein(uebergabeString)) {
					ausgabe = true;
				} else {
					ausgabe = false;
					return ausgabe;
				}
			}
			if (maxlaenge > 0 && uebergabeString.length() > 0 && uebergabeString.length() <= maxlaenge) {
				ausgabe = true;
			} else {
				ausgabe = false;
				return ausgabe;
			}
			if (minLaenge > 0 && uebergabeString.length() > 0 && uebergabeString.length() >= minLaenge) {
				ausgabe = true;
			} else {
				ausgabe = false;
				return ausgabe;
			}

		}

		return ausgabe;

	}

	public static boolean enthealtGrossklein(String uebergabeString) {
		boolean hatGross = false;
		boolean hatKlein = false;

		for (char c : uebergabeString.toCharArray()) {
			if (!hatKlein) {
				hatKlein = Character.isLowerCase(c);
			}
			if (!hatGross) {
				hatGross = Character.isUpperCase(c);
			}

			if (hatKlein && hatGross) {
				return true;
			}

		}

		return false;

	}

	public static boolean enthealtSonderzeiche(String uebergabeString) { // kann nur folgende sonbderzeichen verwalten
																			// /*!@#$%^&*()\"{}_[]|\\?/<>,.
		String specialChars = "/*!@#$%^&*()\"{}_[]|\\?/<>,.";

		for (char c : uebergabeString.toCharArray()) {
			if (specialChars.contains(Character.toString(c))) {
				return true;
			}

		}

		return false;
	}

	public static boolean enthealtZiffer(String uebergabeString) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public static boolean nurziffern(String uebergabeString) {
		return uebergabeString.matches("[+-]?\\d*(\\.\\d+)?");
	}
	
	public static boolean nurziffernwithrange(String uebergabeString, int min, int max) {
		if(uebergabeString.matches("[+-]?\\d*(\\.\\d+)?") == true) {
			int number = Integer.parseInt(uebergabeString);
			if(number >= min && number <= max) {
				return true;
			}else {
				return false;
			}
		}
		return false;
	}
	
	public static boolean checkcategory(String uebergabestring) {
		if(uebergabestring.equals("*")||uebergabestring.equals("**")||uebergabestring.equals("***")||uebergabestring.equals("****")||uebergabestring.equals("*****")) {
			return true;
		}
		return false;
	}
	
	public static boolean isValidMail(String email) {
	      String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	      return email.matches(regex);
	   }
	

}
